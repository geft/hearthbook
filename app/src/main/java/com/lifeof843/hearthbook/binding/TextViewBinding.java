package com.lifeof843.hearthbook.binding;

import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.widget.TextView;

/**
 * Created on 16-Mar-17.
 */

public class TextViewBinding {

    @BindingAdapter("bold")
    public static void setBold(TextView textView, boolean isBold) {
        int typeFace = isBold ? Typeface.BOLD : Typeface.NORMAL;
        textView.setTypeface(null, typeFace);
    }

    @BindingAdapter("color")
    public static void setColor(TextView textView, @ColorRes int colorRes) {
        textView.setTextColor(ContextCompat.getColor(textView.getContext(), colorRes));
    }

    @SuppressWarnings("deprecation")
    @BindingAdapter("html")
    public static void setHtml(TextView textView, @Nullable String html) {
        if (html == null) html = "";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY));
        } else {
            textView.setText(Html.fromHtml(html));
        }
    }
}
