package com.lifeof843.hearthbook.binding;

import android.databinding.BindingAdapter;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.view.View;

/**
 * Created on 16-Mar-17.
 */

public class ViewBinding {

    @BindingAdapter("color")
    public static void setColor(View view, @ColorRes int colorRes) {
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), colorRes));
    }
}
