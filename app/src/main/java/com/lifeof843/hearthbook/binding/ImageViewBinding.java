package com.lifeof843.hearthbook.binding;

import android.databinding.BindingAdapter;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

/**
 * Created on 21-Mar-17.
 */

public class ImageViewBinding {

    @BindingAdapter("srcCompat")
    public static void setImageResource(ImageView imageView, @DrawableRes int resId) {
        imageView.setImageResource(resId);
    }
}
