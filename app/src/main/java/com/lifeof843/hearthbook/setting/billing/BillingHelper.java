package com.lifeof843.hearthbook.setting.billing;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.lifeof843.hearthbook.BuildConfig;
import com.lifeof843.hearthbook.constant.ProductId;
import com.lifeof843.hearthbook.expansion.ExpDownloaderService;
import com.lifeof843.hearthbook.setting.SettingActivity;
import com.lifeof843.hearthbook.util.ExceptionUtil;

/**
 * Created on 29-Mar-17.
 */

public class BillingHelper implements BillingProcessor.IBillingHandler {

    private final Context context;
    private final BillingCallback callback;

    private final BillingProcessor processor;

    public BillingHelper(SettingActivity activity) {
        this.context = activity;
        this.callback = activity;
        this.processor = new BillingProcessor(context, new ExpDownloaderService().getPublicKey(), this);
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        callback.onBillingSuccess();
    }

    @Override
    public void onPurchaseHistoryRestored() {
        if (processor.isPurchased(ProductId.REMOVE_ADS.getCode())) {
            callback.onBillingSuccess();
        }
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        if (BuildConfig.DEBUG) {
            callback.onBillingSuccess();
        } else {
            ExceptionUtil.log("Failed to purchase with error code" + errorCode);
            callback.onBillingFailure();
        }
    }

    @Override
    public void onBillingInitialized() {
        processor.loadOwnedPurchasesFromGoogle();

        if (BillingProcessor.isIabServiceAvailable(context) &&
                processor.isOneTimePurchaseSupported()) {
            callback.onBillingReady();
        } else callback.onBillingFailure();
    }

    public boolean isHandlingActivityResult(int requestCode, int resultCode, Intent data) {
        return processor.handleActivityResult(requestCode, resultCode, data);
    }

    public void release() {
        processor.release();
    }

    @SuppressWarnings("SameParameterValue")
    public void purchase(Activity activity, ProductId id) {
        processor.purchase(activity, id.getCode());
    }
}
