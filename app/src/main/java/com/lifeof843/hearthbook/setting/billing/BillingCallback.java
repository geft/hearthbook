package com.lifeof843.hearthbook.setting.billing;

/**
 * Created on 30-Mar-17.
 */

public interface BillingCallback {

    @SuppressWarnings("EmptyMethod")
    void onBillingReady();

    void onBillingSuccess();

    void onBillingFailure();
}
