package com.lifeof843.hearthbook.setting;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 31-Mar-17.
 */

@Parcel
public class SettingViewModel extends CoreViewModel {

    public ObservableBoolean isUnlocked = new ObservableBoolean();
    public ObservableBoolean isAssetLoaded = new ObservableBoolean();

    public ObservableInt year = new ObservableInt(1);

    public ObservableField<String> selectedLanguage = new ObservableField<>();

    public ObservableField<String> appVersion = new ObservableField<>();
}
