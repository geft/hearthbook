package com.lifeof843.hearthbook.setting;

import android.content.Intent;
import android.support.v7.app.AppCompatDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lifeof843.hearthbook.BuildConfig;
import com.lifeof843.hearthbook.HearthbookApplication;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.CardLanguage;
import com.lifeof843.hearthbook.constant.ProductId;
import com.lifeof843.hearthbook.core.CoreActivity;
import com.lifeof843.hearthbook.databinding.ActivitySettingBinding;
import com.lifeof843.hearthbook.loader.PrefLoader;
import com.lifeof843.hearthbook.setting.billing.BillingCallback;
import com.lifeof843.hearthbook.setting.billing.BillingHelper;
import com.lifeof843.hearthbook.util.ExpansionUtil;
import com.lifeof843.hearthbook.util.SnackBarUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created on 31-Mar-17.
 */

public class SettingActivity extends CoreActivity<ActivitySettingBinding, SettingViewModel>
        implements View.OnClickListener, BillingCallback {

    private BillingHelper billingHelper;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);

        viewModel.year.set(Calendar.getInstance().get(Calendar.YEAR));
        viewModel.selectedLanguage.set(getString(new PrefLoader(this).loadLanguage().getStringRes()));
        viewModel.appVersion.set(BuildConfig.VERSION_NAME);
        viewModel.isUnlocked.set(new PrefLoader(this).isUnlocked());
        viewModel.isAssetLoaded.set(HearthbookApplication.isAssetInitialized());

        initBilling();
    }

    private void initBilling() {
        billingHelper = new BillingHelper(this);
    }

    @Override
    protected SettingViewModel createViewModel() {
        return new SettingViewModel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_share:
                shareAppLink();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openLanguagePicker() {
        AppCompatDialog dialog = new AppCompatDialog(this);
        dialog.setTitle(R.string.setting_language_title);

        List<CardLanguage> languages = new ArrayList<>();
        Collections.addAll(languages, CardLanguage.values());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);

        for (CardLanguage language : languages) {
            adapter.add(getString(language.getStringRes()));
        }

        ListView listView = new ListView(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            CardLanguage selectedLanguage = languages.get(position);
            new PrefLoader(this).saveLanguage(selectedLanguage);
            viewModel.selectedLanguage.set(getString(selectedLanguage.getStringRes()));
            dialog.dismiss();
        });

        dialog.setContentView(listView);
        dialog.show();
    }

    private void shareAppLink() {
        final String appPackageName = getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + appPackageName);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void validateAssets() {
        boolean isAssetLoaded = HearthbookApplication.isAssetInitialized();
        viewModel.isAssetLoaded.set(isAssetLoaded);

        if (isAssetLoaded) {
            SnackBarUtil.show(this, R.string.setting_validate_success);
        } else {
            ExpansionUtil.validateExpansion(this, () -> {
                boolean isAssetLoaded1 = HearthbookApplication.isAssetInitialized();
                if (isAssetLoaded1) {
                    SnackBarUtil.show(this, R.string.setting_validate_success);
                } else {
                    SnackBarUtil.show(
                            SettingActivity.this,
                            R.string.setting_validate_failure,
                            SnackBarUtil.SnackBarType.ERROR);
                }
                viewModel.isAssetLoaded.set(isAssetLoaded1);
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!billingHelper.isHandlingActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        billingHelper.release();

        super.onDestroy();
    }

    @Override
    public void onBillingReady() {

    }

    @Override
    public void onBillingSuccess() {
        new PrefLoader(this).saveUnlock();

        initView();

        SnackBarUtil.show(this, R.string.setting_unlock_success);
    }

    @Override
    public void onBillingFailure() {
        SnackBarUtil.show(this, R.string.setting_unlock_failure, SnackBarUtil.SnackBarType.ERROR);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.containerUnlock)) {
            billingHelper.purchase(this, ProductId.REMOVE_ADS);
        } else if (v.equals(binding.containerLanguage)) {
            openLanguagePicker();
        } else if (v.equals(binding.containerValidate)) {
            validateAssets();
        }
    }
}
