package com.lifeof843.hearthbook.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 09-Dec-17.
 */

public class CardIdData {

    private static List<String> cardIdList = new ArrayList<>();

    public static void setCardIdList(List<String> list) {
        cardIdList.clear();
        cardIdList.addAll(list);
    }

    public static List<String> getCardIdList() {
        return new ArrayList<>(cardIdList);
    }
}
