package com.lifeof843.hearthbook.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16-Mar-17.
 */

@SuppressWarnings("unused")
public enum CardType {
    ALL         ("All Types"),
    HERO        ("Hero"),
    HERO_POWER  ("Hero Power"),
    ENCHANTMENT ("Enchantment"),
    MINION      ("Minion"),
    SPELL       ("Spell"),
    WEAPON      ("Minion");

    private String name;

    CardType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<CardType> getFilterable() {
        List<CardType> list = new ArrayList<>();
        list.add(ALL);
        list.add(MINION);
        list.add(SPELL);
        list.add(WEAPON);
        list.add(HERO);

        return list;
    }
}
