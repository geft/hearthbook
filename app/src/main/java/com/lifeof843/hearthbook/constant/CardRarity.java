package com.lifeof843.hearthbook.constant;

import android.support.annotation.DrawableRes;

import com.lifeof843.hearthbook.R;

/**
 * Created on 16-Mar-17.
 */

public enum CardRarity {
    ALL,
    FREE,
    COMMON      (R.drawable.ic_rarity_common),
    RARE        (R.drawable.ic_rarity_rare),
    EPIC        (R.drawable.ic_rarity_epic),
    LEGENDARY   (R.drawable.ic_rarity_legendary);

    @DrawableRes
    private int iconRes;

    CardRarity() {}

    CardRarity(@DrawableRes int iconRes) {
        this.iconRes = iconRes;
    }

    @DrawableRes
    public int getIconRes() {
        return iconRes;
    }
}
