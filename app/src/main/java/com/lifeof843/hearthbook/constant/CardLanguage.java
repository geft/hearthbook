package com.lifeof843.hearthbook.constant;

import android.support.annotation.StringRes;

import com.lifeof843.hearthbook.R;

/**
 * Created on 07-Apr-17.
 */

public enum CardLanguage {
    ENGLISH(R.string.setting_language_en, "enUS"),
    GERMAN(R.string.setting_language_de, "deDE"),
    ITALIAN(R.string.setting_language_it, "itIT"),
    FRENCH(R.string.setting_language_fr, "frFR"),
    SPANISH_C(R.string.setting_language_es, "esES"),
    SPANISH_M(R.string.setting_language_mx, "esMX"),
    JAPANESE(R.string.setting_language_jp, "jaJP"),
    KOREAN(R.string.setting_language_kr, "koKR"),
    POLISH(R.string.setting_language_pl, "plPL"),
    PORTUGUESE(R.string.setting_language_br, "ptBR"),
    RUSSIAN(R.string.setting_language_ru, "ruRU"),
    THAI(R.string.setting_language_th, "thTH"),
    CHINESE_S(R.string.setting_language_cn, "zhCN"),
    CHINESE_T(R.string.setting_language_tw, "zhTW");

    @StringRes
    private final int stringRes;

    private final String id;

    CardLanguage(int stringRes, String id) {
        this.stringRes = stringRes;
        this.id = id;
    }

    public int getStringRes() {
        return stringRes;
    }

    public String getId() {
        return id;
    }
}
