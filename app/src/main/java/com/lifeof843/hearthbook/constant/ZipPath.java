package com.lifeof843.hearthbook.constant;

/**
 * Created on 29-Mar-17.
 */

public enum ZipPath {
    IMAGE("image/", ".webp"),
    VIDEO("video/", ".webm"),
    AUDIO("audio/", ".ogg"),
    IMAGE_CB("image/cardback_", ".webp"),
    VIDEO_CB("video/cardback_", ".webm"),
    JSON("json/", ".json");

    private final String path;
    private final String ext;

    ZipPath(String path, String ext) {
        this.path = path;
        this.ext = ext;
    }

    public String getPath() {
        return path;
    }

    public String getExt() {
        return ext;
    }
}
