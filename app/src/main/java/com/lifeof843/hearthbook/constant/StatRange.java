package com.lifeof843.hearthbook.constant;

/**
 * Created on 26-Mar-17.
 */

public enum StatRange {
    COST(0, 9),
    ATTACK(0, 9),
    HEALTH(0, 9);

    private final int min;
    private final int max;

    StatRange(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
