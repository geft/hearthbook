package com.lifeof843.hearthbook.constant;

/**
 * Created on 23-Mar-17.
 */

public enum CardRace {
    ALL,
    BEAST,
    DEMON,
    DRAGON,
    ELEMENTAL,
    MURLOC,
    MECHANICAL,
    PIRATE,
    TOTEM
}
