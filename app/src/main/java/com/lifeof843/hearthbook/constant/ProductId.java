package com.lifeof843.hearthbook.constant;

/**
 * Created on 30-Mar-17.
 */

public enum ProductId {
    REMOVE_ADS("remove_ads");

    private final String code;

    ProductId(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
