package com.lifeof843.hearthbook.constant;

import android.support.annotation.DrawableRes;

import com.lifeof843.hearthbook.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16-Mar-17.
 */

public enum CardClass {
    ALL             (R.drawable.ic_empty,           "All Classes"),
    DEATHKNIGHT     (R.drawable.ic_empty,           "Death Knight"),
    DREAM           (R.drawable.ic_empty,           "Dream"),
    NEUTRAL         (R.drawable.ic_empty,           "Neutral"),
    DRUID           (R.drawable.ic_class_druid,     "Druid"),
    HUNTER          (R.drawable.ic_class_hunter,    "Hunter"),
    MAGE            (R.drawable.ic_class_mage,      "Mage"),
    PALADIN         (R.drawable.ic_class_paladin,   "Paladin"),
    PRIEST          (R.drawable.ic_class_priest,    "Priest"),
    ROGUE           (R.drawable.ic_class_rogue,     "Rogue"),
    SHAMAN          (R.drawable.ic_class_shaman,    "Shaman"),
    WARLOCK         (R.drawable.ic_class_warlock,   "Warlock"),
    WARRIOR         (R.drawable.ic_class_warrior,   "Warrior");

    @DrawableRes
    private int iconRes;

    private String name;

    CardClass(int iconRes, String name) {
        this.iconRes = iconRes;
        this.name = name;
    }

    public int getIconRes() {
        return iconRes;
    }

    public String getName() {
        return name;
    }

    public static List<CardClass> getFilterable() {
        List<CardClass> list = new ArrayList<>();
        list.add(ALL);
        list.add(NEUTRAL);
        list.add(DRUID);
        list.add(HUNTER);
        list.add(MAGE);
        list.add(PALADIN);
        list.add(PRIEST);
        list.add(ROGUE);
        list.add(SHAMAN);
        list.add(WARLOCK);

        return list;
    }
}
