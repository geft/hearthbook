package com.lifeof843.hearthbook.constant;

import android.support.annotation.DrawableRes;

import com.lifeof843.hearthbook.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16-Mar-17.
 */

@SuppressWarnings("unused")
public enum CardSet {
    ALL         (R.drawable.ic_empty,       "All Sets"),
    CORE        (R.drawable.ic_empty,       "Core"),
    HOF         (R.drawable.ic_empty,       "Hall of Fame"),
    TB          (R.drawable.ic_empty,       "Tavern Brawl"),
    CREDITS     (R.drawable.ic_empty,       "Credits"),
    HERO_SKINS  (R.drawable.ic_empty,       "Hero Skins"),
    MISSIONS    (R.drawable.ic_empty,       "Missions"),
    EXPERT1     (R.drawable.ic_set_classic, "Classic"),
    NAXX        (R.drawable.ic_set_naxx,    "Curse of Naxxramas"),
    BRM         (R.drawable.ic_set_brm,     "Blackrock Mountain"),
    KARA        (R.drawable.ic_set_kara,    "One Night in Karazhan"),
    GVG         (R.drawable.ic_set_gvg,     "Goblins vs Gnomes"),
    TGT         (R.drawable.ic_set_tgt,     "The Grand Tournament"),
    LOE         (R.drawable.ic_set_loe,     "League of Explorers"),
    OG          (R.drawable.ic_set_og,      "Whispers of the Old Gods"),
    GANGS       (R.drawable.ic_set_gangs,   "Mean Streets of Gadgetzan"),
    UNGORO      (R.drawable.ic_set_ungoro,  "Journey to Un'goro"),
    ICECROWN    (R.drawable.ic_set_icc,     "Knights of the Frozen Throne"),
    LOOTAPALOOZA(R.drawable.ic_set_loot,    "Kobolds and Catacombs");

    @DrawableRes
    private int iconRes;

    private String name;

    CardSet(int iconRes, String name) {
        this.iconRes = iconRes;
        this.name = name;
    }

    public int getIconRes() {
        return iconRes;
    }

    public String getName() {
        return name;
    }

    public static List<CardSet> getStandard() {
        List<CardSet> list = new ArrayList<>();
        list.add(CORE);
        list.add(EXPERT1);
        list.add(OG);
        list.add(KARA);
        list.add(GANGS);
        list.add(UNGORO);
        list.add(ICECROWN);
        list.add(LOOTAPALOOZA);

        return list;
    }

    public static List<CardSet> getFilterable() {
        List<CardSet> list = new ArrayList<>();

        list.add(ALL);
        list.add(CORE);
        list.add(HOF);
        list.add(EXPERT1);
        list.add(NAXX);
        list.add(BRM);
        list.add(KARA);
        list.add(GVG);
        list.add(TGT);
        list.add(LOE);
        list.add(OG);
        list.add(GANGS);
        list.add(UNGORO);
        list.add(ICECROWN);
        list.add(LOOTAPALOOZA);

        return list;
    }
}
