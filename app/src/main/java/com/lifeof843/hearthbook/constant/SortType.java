package com.lifeof843.hearthbook.constant;

/**
 * Created on 23-Mar-17.
 */

public enum SortType {
    ID,
    NAME,
    COST,
    ATTACK,
    HEALTH
}
