package com.lifeof843.hearthbook.constant;

import android.support.annotation.DrawableRes;

import com.lifeof843.hearthbook.R;

/**
 * Created on 16-Mar-17.
 */

public enum CardGang {
    ALL(R.drawable.ic_empty, "All Gangs"),
    GRIMY_GOONS(R.drawable.ic_gangs_goons, "Grimy Goons"),
    JADE_LOTUS(R.drawable.ic_gangs_lotus, "Jade Lotus"),
    KABAL(R.drawable.ic_gangs_kabal, "Kabal");

    @DrawableRes
    private int iconRes;

    private String name;

    CardGang(int iconRes, String name) {
        this.iconRes = iconRes;
        this.name = name;
    }

    public int getIconRes() {
        return iconRes;
    }

    public String getName() {
        return name;
    }
}
