package com.lifeof843.hearthbook.constant;

/**
 * Created on 25-Mar-17.
 */

public enum CardFilter {
    DEFAULT,
    COLLECTIBLE,
    ID,
    NAME,
    SET,
    RARITY,
    CLASS,
    GANG,
    RACE,
    TYPE,
    COST,
    ATTACK,
    HEALTH,
    TEXT
}
