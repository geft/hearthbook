package com.lifeof843.hearthbook.constant;

/**
 * Created on 16-Mar-17.
 */

public interface Duration {
    int SHORT       = 250;
    int VERY_LONG   = 2000;
}
