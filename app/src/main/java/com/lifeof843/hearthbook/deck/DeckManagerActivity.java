package com.lifeof843.hearthbook.deck;

import android.support.v7.widget.LinearLayoutManager;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreActivity;
import com.lifeof843.hearthbook.core.recycle_view.BindAdapter;
import com.lifeof843.hearthbook.databinding.ActivityDeckManagerBinding;

import java.util.List;

/**
 * Created on 25-Apr-17.
 */

public class DeckManagerActivity extends CoreActivity<ActivityDeckManagerBinding, DeckManagerViewModel> {

    private DeckGenerator generator;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_deck_manager;
    }

    @Override
    protected void initView() {
        generator = new DeckGenerator(this);

        initFab();
        initRecyclerView();
    }

    @Override
    protected DeckManagerViewModel createViewModel() {
        return new DeckManagerViewModel();
    }

    private void initFab() {
        binding.fab.setOnClickListener(v -> generator.addDeck());
    }

    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(getBindAdapter(generator.getDeckList()));
    }

    private BindAdapter<Deck> getBindAdapter(List<Deck> deckList) {
        return new BindAdapter<Deck>(this, R.layout.widget_deck, deckList) {
            @Override
            public void onItemClick(Deck item, int position) {

            }
        };
    }
}
