package com.lifeof843.hearthbook.deck;

import android.support.annotation.DrawableRes;

import com.bluelinelabs.logansquare.annotation.JsonObject;
import com.lifeof843.hearthbook.constant.CardClass;
import com.lifeof843.hearthbook.model.IconPicker;

import org.parceler.Parcel;

/**
 * Created on 25-Apr-17.
 */

@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS)
public class Deck {

    public CardClass cardClass;
    public String name;
    public String description;

    public boolean showDescription() {
        return description != null && !description.isEmpty();
    }

    @DrawableRes
    public int getClassIcon() {
        return IconPicker.getClassIcon(cardClass);
    }
}
