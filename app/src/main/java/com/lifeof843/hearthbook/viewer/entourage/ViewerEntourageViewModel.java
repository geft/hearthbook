package com.lifeof843.hearthbook.viewer.entourage;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 05-Apr-17.
 */

@Parcel
public class ViewerEntourageViewModel extends CoreViewModel {
    ViewerEntourageData data;
}
