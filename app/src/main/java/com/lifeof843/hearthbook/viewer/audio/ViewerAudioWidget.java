package com.lifeof843.hearthbook.viewer.audio;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetViewerAudioBinding;
import com.lifeof843.hearthbook.loader.ExpLoader;
import com.lifeof843.hearthbook.model.CardSound;
import com.lifeof843.hearthbook.util.ExceptionUtil;

import java.io.IOException;
import java.util.List;

/**
 * Created on 05-Apr-17.
 */

public class ViewerAudioWidget extends CoreWidget<ViewerAudioData, WidgetViewerAudioBinding, ViewerAudioViewModel> {

    public ViewerAudioWidget(@NonNull Context context) {
        super(context);
    }

    public ViewerAudioWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_viewer_audio;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected ViewerAudioViewModel createViewModel() {
        return new ViewerAudioViewModel();
    }

    @Override
    public void setData(ViewerAudioData data) {
        viewModel.data = data;

        initAudioList(data.cardId);
    }

    @Nullable
    @Override
    public ViewerAudioData getData() {
        return viewModel.data;
    }

    private void initAudioList(String cardId) {
        List<CardSound> sounds = new ExpLoader(getContext()).getAudioList(cardId);

        int padding = getContext().getResources().getDimensionPixelOffset(R.dimen.padding_half);

        if (sounds.isEmpty()) {
            binding.getRoot().setVisibility(GONE);
        } else {
            for (CardSound sound : sounds) {
                createSoundButton(cardId, padding, sound);
            }
        }
    }

    private void createSoundButton(String cardId, int padding, CardSound sound) {
        ImageButton button = new ImageButton(getContext());
        button.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_audio));
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setPadding(padding, 0, padding, 0);
        button.setOnClickListener(v -> playSound(sound));
        button.setOnLongClickListener(v -> {
            if (viewModel.data.callback != null) {
                viewModel.data.callback.openAudioDialog(cardId, sound);
            }

            return true;
        });

        ExceptionUtil.log("Add audio: " + sound.getPath());
        binding.container.addView(button);
    }

    private void playSound(CardSound cardSound) {
        try {
            MediaPlayer mp = new MediaPlayer();
            mp.setOnPreparedListener(MediaPlayer::start);
            mp.setOnCompletionListener(MediaPlayer::release);
            mp.setDataSource(
                    cardSound.getDescriptor().getFileDescriptor(),
                    cardSound.getDescriptor().getStartOffset(),
                    cardSound.getDescriptor().getLength());

            mp.prepare();
        } catch (IOException e) {
            ExceptionUtil.log(e,
                    getContext().getString(R.string.error_load_path_format, cardSound.getPath()));
        }
    }
}
