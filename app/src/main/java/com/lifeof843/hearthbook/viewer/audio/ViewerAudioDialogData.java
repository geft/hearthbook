package com.lifeof843.hearthbook.viewer.audio;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.parceler.Parcel;

/**
 * Created on 05-Apr-17.
 */

@Parcel
public class ViewerAudioDialogData {

    @Nullable
    public Uri uri;

    @NonNull
    public String id = "";

    public long length;
}
