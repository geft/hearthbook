package com.lifeof843.hearthbook.viewer.image;

import android.databinding.ObservableBoolean;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 02-Apr-17.
 */

@Parcel
public class ViewerImageViewModel extends CoreViewModel {

    public ObservableBoolean isLoaded = new ObservableBoolean();
    public ObservableBoolean hasAnimation = new ObservableBoolean();
    public ObservableBoolean isCollectible = new ObservableBoolean();

    ViewerImageData data;
}
