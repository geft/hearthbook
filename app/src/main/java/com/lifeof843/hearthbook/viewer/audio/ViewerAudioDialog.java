package com.lifeof843.hearthbook.viewer.audio;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.view.LayoutInflater;

import com.google.android.vending.expansion.zipfile.ZipResourceFile;
import com.lifeof843.hearthbook.HearthbookApplication;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.ZipPath;
import com.lifeof843.hearthbook.databinding.WidgetAudioDialogBinding;
import com.lifeof843.hearthbook.util.ExceptionUtil;
import com.lifeof843.hearthbook.util.SnackBarUtil;
import com.lifeof843.hearthbook.viewer.ViewerActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created on 05-Apr-17.
 */

public class ViewerAudioDialog extends AppCompatDialog {

    private WidgetAudioDialogBinding binding;

    public ViewerAudioDialog(Context context) {
        super(context);

        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.inflate(
                LayoutInflater.from(getContext()), R.layout.widget_audio_dialog, null, false);

        setContentView(binding.getRoot());
    }

    public void setData(ViewerAudioDialogData data) {
        binding.buttonRingtone.setDelayedClickListener(() -> handleClick(data, RingtoneManager.TYPE_RINGTONE));
        binding.buttonNotification.setDelayedClickListener(() -> handleClick(data, RingtoneManager.TYPE_NOTIFICATION));
        binding.buttonAlarm.setDelayedClickListener(() -> handleClick(data, RingtoneManager.TYPE_ALARM));
    }

    private void handleClick(ViewerAudioDialogData data, final int soundType) {
        if (isExternalStorageWritable()) {
            setRingtone(data, soundType);
        } else {
            SnackBarUtil.show(getContext(), R.string.error_external_storage, SnackBarUtil.SnackBarType.ERROR);
        }

        dismiss();
    }

    private void setRingtone(ViewerAudioDialogData data, int soundType) {
        if (getOwnerActivity() == null) return;

        Uri uri = data.uri;

        File file = null;
        if (uri != null) {
            String path = ZipPath.AUDIO.getPath() + uri.getLastPathSegment();
            file = getFile(path, uri.getLastPathSegment());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.System.canWrite(getContext())) {
            getOwnerActivity().startActivityForResult(
                    new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS),
                    ViewerActivity.PERMISSION_WRITE_SETTINGS);
        } else if (file != null && getOwnerActivity() != null) {
            performSetRingtone(data, soundType, file);
        } else {
            showFailSnackBar();
        }
    }

    private void showSuccessSnackBar() {
        SnackBarUtil.show(getOwnerActivity(), R.string.viewer_audio_set_success);
    }

    private void showFailSnackBar() {
        SnackBarUtil.show(getOwnerActivity(), R.string.viewer_audio_set_fail);
    }

    private void performSetRingtone(ViewerAudioDialogData data, int soundType, File file) {
        if (getOwnerActivity() == null) return;

        Uri baseUri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        deleteDataIfExists(baseUri, file);

        ContentValues values = getRingtoneContentValues(file, data.id, data.length);
        Uri newUri = getOwnerActivity().getContentResolver().insert(baseUri, values);

        RingtoneManager.setActualDefaultRingtoneUri(getOwnerActivity(), soundType, newUri);
        showSuccessSnackBar();
    }

    private void deleteDataIfExists(Uri baseUri, File file) {
        if (file == null) return;

        getContext().getContentResolver().delete(
                baseUri,
                MediaStore.MediaColumns.DATA + "=\""
                + file.getAbsolutePath() + "\"", null);
    }

    private ContentValues getRingtoneContentValues(File file, String id, long length) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, id);
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/ogg");
        values.put(MediaStore.MediaColumns.SIZE, length);
        values.put(MediaStore.Audio.Media.ARTIST, "Blizzard Entertainment Inc.");
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_ALARM, true);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);
        return values;
    }

    @Nullable
    private File getFile(String path, String name) {
        try {
            InputStream input;

            ZipResourceFile zipResourceFile = HearthbookApplication.zipResourceFile;

            if (zipResourceFile == null) return null;
            else input = zipResourceFile.getInputStream(path);

            File file = new File(getContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), name);
            try (OutputStream output = new FileOutputStream(file)) {
                final byte[] buffer = new byte[1024];
                int read;

                while ((read = input.read(buffer)) != -1)
                    output.write(buffer, 0, read);

                output.flush();
            }

            input.close();

            return file;
        } catch (IOException e) {
            ExceptionUtil.log(e);
            return null;
        }
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }
}
