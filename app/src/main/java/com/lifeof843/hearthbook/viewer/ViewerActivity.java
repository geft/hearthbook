package com.lifeof843.hearthbook.viewer;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.f2prateek.dart.InjectExtra;
import com.google.android.gms.ads.AdListener;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.browser.SortProcessor;
import com.lifeof843.hearthbook.constant.SortType;
import com.lifeof843.hearthbook.core.CoreActivity;
import com.lifeof843.hearthbook.data.CardIdData;
import com.lifeof843.hearthbook.databinding.ActivityViewerBinding;
import com.lifeof843.hearthbook.loader.AssetLoader;
import com.lifeof843.hearthbook.loader.PrefLoader;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.model.CardSound;
import com.lifeof843.hearthbook.util.AdUtil;
import com.lifeof843.hearthbook.util.ExceptionUtil;
import com.lifeof843.hearthbook.util.ObservableUtil;
import com.lifeof843.hearthbook.viewer.audio.ViewerAudioDialog;
import com.lifeof843.hearthbook.viewer.audio.ViewerAudioDialogData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created on 17-Mar-17.
 */

public class ViewerActivity extends CoreActivity<ActivityViewerBinding, ViewerViewModel> implements ViewerCallback {

    @InjectExtra
    int position;

    @InjectExtra
    SortType sortType;

    @InjectExtra
    boolean isEntourage;

    public static final int PERMISSION_WRITE_SETTINGS = 100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition();
        }

        initUnlock();

    }

    private void initUnlock() {
        boolean isUnlocked = new PrefLoader(this).isUnlocked();
        viewModel.isUnlocked.set(isUnlocked);

        if (!isUnlocked) {
            binding.adView.loadAd(AdUtil.getAdRequest());
            binding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);

                    binding.adView.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_viewer;
    }

    @Override
    protected void initView() {
        initButtons();
        initAssets();
    }

    private void initButtons() {
        binding.chevronLeft.setOnClickListener(v ->
                binding.viewPager.setCurrentItem(getCurrentItem() - 1, true));

        binding.chevronRight.setOnClickListener(v ->
                binding.viewPager.setCurrentItem(getCurrentItem() + 1, true));
    }

    private int getCurrentItem() {
        return binding.viewPager.getCurrentItem();
    }

    private void initAssets() {
        List<Card> list = Collections.synchronizedList(new ArrayList<>());
        List<String> cardIdList = CardIdData.getCardIdList();

        try {
            disposable.add(
                    Observable.fromIterable(new AssetLoader(this).getAllCards())
                            .flatMap(card -> Observable.just(card)
                                    .map(card1 -> {
                                        if (cardIdList.contains(card1.getId())) {
                                            list.add(card);
                                        }
                                        return card1;
                                    })
                            )
                            .toList()
                            .map(cards -> new SortProcessor().sort(sortType, list))
                            .compose(ObservableUtil.applyCommonSchedulers())
                            .map(this::initPager)
                            .subscribe(cards -> beginTransition(), ExceptionUtil::log)

            );
        } catch (IOException e) {
            mapError(new IOException(getString(R.string.error_load_asset)));
        }
    }

    private List<Card> initPager(List<Card> cards) {
        ViewerAdapter adapter = new ViewerAdapter(this, cards);
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setCurrentItem(viewModel.currPosition == null ? position : viewModel.currPosition);
        binding.viewPager.setOffscreenPageLimit(2);
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                viewModel.currPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        return cards;
    }

    private void beginTransition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            View viewWithTag = binding.viewPager.findViewWithTag(position);

            if (viewWithTag != null) {
                viewWithTag.findViewById(R.id.browser_item)
                        .setTransitionName(isEntourage ?
                                getString(R.string.transition_card_entourage) :
                                getString(R.string.transition_card_name));
                startPostponedEnterTransition();
            }
        }
    }

    @Override
    protected ViewerViewModel createViewModel() {
        return new ViewerViewModel();
    }

    @Override
    public void openAudioDialog(String cardId, CardSound sound) {
        ViewerAudioDialogData data = new ViewerAudioDialogData();
        data.uri = Uri.parse(sound.getPath());
        data.id = cardId;
        data.length = sound.getDescriptor().getLength();

        viewModel.audioDialogData = data;

        ViewerAudioDialog audioDialog = new ViewerAudioDialog(this);
        audioDialog.show();
        audioDialog.setData(data);
    }
}
