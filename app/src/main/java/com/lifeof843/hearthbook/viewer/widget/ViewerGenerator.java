package com.lifeof843.hearthbook.viewer.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.Duration;
import com.lifeof843.hearthbook.databinding.WidgetViewerBinding;
import com.lifeof843.hearthbook.loader.AssetLoader;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.model.CardEntourage;
import com.lifeof843.hearthbook.util.ExceptionUtil;
import com.lifeof843.hearthbook.util.ObservableUtil;
import com.lifeof843.hearthbook.util.StringUtil;
import com.lifeof843.hearthbook.viewer.ViewerCallback;
import com.lifeof843.hearthbook.viewer.audio.ViewerAudioData;
import com.lifeof843.hearthbook.viewer.audio.ViewerAudioWidget;
import com.lifeof843.hearthbook.viewer.entourage.ViewerEntourageData;
import com.lifeof843.hearthbook.viewer.entourage.ViewerEntourageWidget;
import com.lifeof843.hearthbook.viewer.image.ViewerImageData;
import com.lifeof843.hearthbook.viewer.image.ViewerImageWidget;
import com.lifeof843.hearthbook.viewer.simple.ViewerSimpleData;
import com.lifeof843.hearthbook.viewer.simple.ViewerSimpleWidget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created on 17-Mar-17.
 */

class ViewerGenerator {

    private final Context context;
    private final WidgetViewerBinding binding;
    private final ViewerCallback callback;

    ViewerGenerator(Context context, WidgetViewerBinding binding, ViewerCallback callback) {
        this.context = context;
        this.binding = binding;
        this.callback = callback;
    }

    void init(Card card) {
        if (binding.browserItem == null) return;

        binding.browserItem.setItem(card);
        binding.getRoot().post(() -> populateViews(card));
    }

    private void populateViews(Card card) {
        Single.zip(
                Single.just(getImageWidget(card)).subscribeOn(Schedulers.computation()),
                Single.just(getViewsFromData(getSimpleData(card))).subscribeOn(Schedulers.computation()),
                Single.just(getAudioWidget(card.getId())).subscribeOn(Schedulers.computation()),
                Single.just(getEntourageWidget(card)).subscribeOn(Schedulers.computation()),
                (image, simple, audio, entourage) -> {
                    List<View> views = new ArrayList<>();
                    views.add(image);
                    views.addAll(simple);
                    views.add(audio);
                    views.add(entourage);
                    views.add(getPadding());

                    return views;
                }
        )
                .compose(ObservableUtil.applyCommonSchedulers())
                .subscribe(this::addViews, ExceptionUtil::log);
    }

    private View getPadding() {
        View view = new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.padding_huge)
        ));
        return view;
    }

    @NonNull
    private ViewerImageWidget getImageWidget(Card card) {
        ViewerImageData data = new ViewerImageData();
        data.setCollectible(card.isCollectible());
        data.setId(card.getId());

        ViewerImageWidget widget = new ViewerImageWidget(context);
        widget.setData(data);
        return widget;
    }

    private View getAudioWidget(String id) {
        if (id == null) return new View(context);

        ViewerAudioData data = new ViewerAudioData();
        data.cardId = id;
        data.callback = callback;

        ViewerAudioWidget widget = new ViewerAudioWidget(context);
        widget.setData(data);

        return widget;
    }

    private View getEntourageWidget(Card card) {
        ViewerEntourageData data = new ViewerEntourageData();

        try {
            List<CardEntourage> entourageList = new AssetLoader(context).getEntourage();
            for (CardEntourage entourage : entourageList) {
                if (entourage.id.equals(card.getId())) {
                    data.cardIds.addAll(entourage.entourage);
                    break;
                }
            }
        } catch (IOException e) {
            ExceptionUtil.log(e);
        }

        if (card.getEntourage() != null) {
            data.cardIds.addAll(card.getEntourage());
        }

        if (data.cardIds.isEmpty()) return new View(context);

        ViewerEntourageWidget widget = new ViewerEntourageWidget(context);
        widget.setData(data);

        return widget;
    }

    @NonNull
    private List<ViewerSimpleData> getSimpleData(Card card) {
        List<ViewerSimpleData> list = new ArrayList<>();
        list.add(getSimpleData(R.string.viewer_text, card.getText()));
        list.add(getSimpleData(R.string.viewer_race, StringUtil.capitalizeFirst(card.getRace())));
        list.add(getSimpleData(R.string.viewer_set, card.getSetLabel()));
        list.add(getSimpleData(R.string.viewer_flavor, card.getFlavor()));
        list.add(getSimpleData(R.string.viewer_artist, card.getArtist()));

        return list;
    }

    @SuppressWarnings("Convert2streamapi")
    @NonNull
    private List<View> getViewsFromData(List<ViewerSimpleData> dataList) {
        List<View> viewList = new ArrayList<>();
        for (ViewerSimpleData data : dataList) {
            if (data.label != null && data.content != null && !data.content.isEmpty()) {
                ViewerSimpleWidget widget = new ViewerSimpleWidget(context);
                widget.setData(data);
                viewList.add(widget);
            }
        }
        return viewList;
    }

    private void addViews(List<View> viewList) {
        new Handler().post(() -> {
            for (View view : viewList) {
                getAnimator(view).start();
            }
        });
    }

    private ValueAnimator getAnimator(final View view) {
        ValueAnimator animator = ValueAnimator.ofInt(500, 0);
        animator.setDuration(Duration.SHORT);
        animator.setStartDelay(Duration.SHORT);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setAlpha(0);
                binding.container.addView(view);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setAlpha(1);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                view.setAlpha(1);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.addUpdateListener(animation -> {
            view.setAlpha(animation.getAnimatedFraction());
            view.setTranslationY((int) animation.getAnimatedValue());
        });

        return animator;
    }

    private ViewerSimpleData getSimpleData(@StringRes int label, String content) {
        ViewerSimpleData data = new ViewerSimpleData();
        data.label = context.getString(label);
        data.content = content;

        return data;
    }
}
