package com.lifeof843.hearthbook.viewer.image;

import android.support.annotation.Nullable;

import org.parceler.Parcel;

/**
 * Created on 02-Apr-17.
 */

@Parcel
public class ViewerImageData {

    boolean isCollectible;

    String id = "";

    @Nullable
    Integer cardBackId;

    public boolean isCollectible() {
        return isCollectible;
    }

    public void setCollectible(boolean collectible) {
        isCollectible = collectible;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Nullable
    public Integer getCardBackId() {
        return cardBackId;
    }

    public void setCardBackId(@Nullable Integer cardBackId) {
        this.cardBackId = cardBackId;
    }
}
