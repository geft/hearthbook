package com.lifeof843.hearthbook.viewer.image;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.VideoView;

import com.google.android.vending.expansion.zipfile.ZipResourceFile;
import com.jakewharton.rxbinding2.view.RxView;
import com.lifeof843.hearthbook.HearthbookApplication;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.ZipPath;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetViewerAnimBinding;
import com.lifeof843.hearthbook.databinding.WidgetViewerImageBinding;
import com.lifeof843.hearthbook.expansion.ExpProvider;
import com.lifeof843.hearthbook.loader.ExpLoader;
import com.lifeof843.hearthbook.util.ExceptionUtil;

import java.io.File;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created on 02-Apr-17.
 */

public class ViewerImageWidget extends CoreWidget<ViewerImageData, WidgetViewerImageBinding, ViewerImageViewModel> {

    public ViewerImageWidget(@NonNull Context context) {
        super(context);
    }

    public ViewerImageWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_viewer_image;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected ViewerImageViewModel createViewModel() {
        return new ViewerImageViewModel();
    }

    @Override
    public void setData(ViewerImageData data) {
        viewModel.isCollectible.set(data.isCollectible());

        String imagePath;

        if (data.getCardBackId() != null) {
            imagePath = ZipPath.IMAGE_CB.getPath() + data.getCardBackId().toString() + ZipPath.IMAGE_CB.getExt();
        } else {
            imagePath = ZipPath.IMAGE.getPath() + data.getId() + ZipPath.IMAGE.getExt();
        }

        ExceptionUtil.log(imagePath);
        Drawable drawable = new ExpLoader(getContext()).getDrawableFromPath(imagePath);

        viewModel.isLoaded.set(drawable != null);
        binding.imageCard.setImageDrawable(drawable);

        initAnimation(data);
    }

    @Nullable
    @Override
    public ViewerImageData getData() {
        return viewModel.data;
    }

    private void initAnimation(ViewerImageData data) {
        String videoPath;

        if (data.getCardBackId() != null) {
            videoPath = ZipPath.VIDEO_CB.getPath() + data.getCardBackId() + ZipPath.VIDEO_CB.getExt();
        } else {
            videoPath = ZipPath.VIDEO.getPath() + data.getId() + ZipPath.VIDEO.getExt();
        }

        if (doesAnimationExist(videoPath)) {
            viewModel.hasAnimation.set(true);
            RxView.clicks(binding.imageCard)
                    .throttleFirst(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> playAnimation(getURI(videoPath)), ExceptionUtil::log);
        }
    }

    private boolean doesAnimationExist(String filePath) {
        ZipResourceFile file = HearthbookApplication.zipResourceFile;

        return file != null && file.getAssetFileDescriptor(filePath) != null;
    }

    private Uri getURI(String filePath) {
        return Uri.parse(getContentUri() + File.separator + filePath);
    }

    private Uri getContentUri() {
        return Uri.parse("content://" + new ExpProvider().getAuthority());
    }

    private void playAnimation(final Uri uri) {
        AppCompatDialog dialog = new AppCompatDialog(getContext(), R.style.VideoDialog);

        WidgetViewerAnimBinding animBinding = WidgetViewerAnimBinding.inflate(
                LayoutInflater.from(getContext()));

        VideoView videoView = animBinding.videoView;
        videoView.setZOrderOnTop(true);
        videoView.setVideoURI(uri);
        videoView.setOnPreparedListener(mp -> {
            mp.setLooping(true);
            mp.start();
        });
        videoView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                dialog.dismiss();
            }
            return true;
        });

        dialog.setContentView(animBinding.getRoot());
        dialog.show();
    }
}
