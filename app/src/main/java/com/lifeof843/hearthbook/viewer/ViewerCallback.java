package com.lifeof843.hearthbook.viewer;

import com.lifeof843.hearthbook.model.CardSound;

/**
 * Created on 05-Apr-17.
 */

public interface ViewerCallback {

    void openAudioDialog(String cardId, CardSound sound);
}
