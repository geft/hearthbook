package com.lifeof843.hearthbook.viewer.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetViewerBinding;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.viewer.ViewerCallback;

/**
 * Created on 17-Mar-17.
 */

public class ViewerWidget extends CoreWidget<Card, WidgetViewerBinding, ViewerWidgetViewModel> {

    @Nullable
    private ViewerCallback callback;

    public ViewerWidget(@NonNull Context context, @Nullable ViewerCallback callback) {
        super(context);

        this.callback = callback;
    }

    public ViewerWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_viewer;
    }

    @Override
    protected void initView() {
    }

    @Override
    protected ViewerWidgetViewModel createViewModel() {
        return new ViewerWidgetViewModel();
    }

    @Override
    public void setData(Card card) {
        viewModel.card = card;
        new ViewerGenerator(getContext(), binding, callback).init(card);
    }

    @Nullable
    @Override
    public Card getData() {
        return viewModel.card;
    }

}
