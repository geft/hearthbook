package com.lifeof843.hearthbook.viewer.entourage;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.lifeof843.hearthbook.Henson;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.browser.SortProcessor;
import com.lifeof843.hearthbook.constant.SortType;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.data.CardIdData;
import com.lifeof843.hearthbook.databinding.WidgetBrowserItemBinding;
import com.lifeof843.hearthbook.databinding.WidgetViewerEntourageBinding;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.filterable.FilterableProcessor;
import com.lifeof843.hearthbook.filterable.type.FilterId;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.util.FilterableUtil;
import com.lifeof843.hearthbook.util.ObservableUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created on 05-Apr-17.
 */

public class ViewerEntourageWidget extends CoreWidget<ViewerEntourageData, WidgetViewerEntourageBinding, ViewerEntourageViewModel> {

    public ViewerEntourageWidget(@NonNull Context context) {
        super(context);
    }

    public ViewerEntourageWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_viewer_entourage;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected ViewerEntourageViewModel createViewModel() {
        return new ViewerEntourageViewModel();
    }

    @Override
    public void setData(ViewerEntourageData data) {
        viewModel.data = data;
        
        initEntourageItems(data.cardIds);
    }

    @Nullable
    @Override
    public ViewerEntourageData getData() {
        return viewModel.data;
    }

    @SuppressWarnings("Convert2streamapi")
    private void initEntourageItems(List<String> entourage) {
        List<Filterable> filterableList = new ArrayList<>();

        for (String string : entourage) {
            filterableList.add(new FilterId(string));
        }

        disposable.add(
                new FilterableProcessor(getContext(), FilterableUtil.mergeList(filterableList)).applyFiltersFromAsset()
                        .map(cards -> new SortProcessor().sort(SortType.ID, cards))
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(cards -> addViews(entourage, cards))
        );
    }

    private void addViews(List<String> entourage, List<Card> cards) {
        if (cards.isEmpty()) return;

        disposable.add(
                Observable.range(0, cards.size())
                        .flatMap(integer -> Observable.just(integer)
                                .map(integer1 -> {
                                    Card card = cards.get(integer1);
                                    WidgetBrowserItemBinding itemBinding = DataBindingUtil.inflate(
                                            LayoutInflater.from(getContext()), R.layout.widget_browser_item, null, true);
                                    itemBinding.setItem(card);
                                    itemBinding.viewClass.setBackgroundColor(ContextCompat.getColor(getContext(), card.getColorFromClass()));

                                    View root = itemBinding.getRoot();
                                    root.setOnClickListener(v -> getClickListener(entourage, root, integer1));

                                    return root;
                                })
                        )
                        .toList()
                        .subscribe(viewList -> {
                            for (View view : viewList) binding.container.addView(view);
                        })
        );
    }

    private void getClickListener(List<String> entourage, View root, int tempIndex) {
        CardIdData.setCardIdList(entourage);

        Intent intent = Henson.with(getContext())
                .gotoViewerActivity()
                .isEntourage(true)
                .position(tempIndex)
                .sortType(SortType.ID)
                .build();

        // todo anim
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Single.just(AnimUtil.getTransitionBundleToViewer(root, R.string.transition_card_entourage, null))
//                    .compose(ObservableUtil.applyCommonSchedulers())
//                    .subscribe(bundle -> getContext().startActivity(intent, bundle),
//                            ExceptionUtil::log);
//        } else {
//            getContext().startActivity(intent);
//        }

        getContext().startActivity(intent);
    }
}
