package com.lifeof843.hearthbook.viewer.widget;

import com.lifeof843.hearthbook.core.CoreViewModel;
import com.lifeof843.hearthbook.model.Card;

import org.parceler.Parcel;

/**
 * Created on 17-Mar-17.
 */

@Parcel
public class ViewerWidgetViewModel extends CoreViewModel {

    Card card;
}
