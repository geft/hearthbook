package com.lifeof843.hearthbook.viewer.entourage;

import android.support.annotation.NonNull;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 05-Apr-17.
 */

@Parcel
public class ViewerEntourageData {

    @NonNull
    public List<String> cardIds = new ArrayList<>();
}
