package com.lifeof843.hearthbook.viewer.simple;

import android.databinding.ObservableField;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 17-Mar-17.
 */

@Parcel
public class ViewerSimpleViewModel extends CoreViewModel {

    public ObservableField<String> label = new ObservableField<>();

    public ObservableField<String> content = new ObservableField<>();
}
