package com.lifeof843.hearthbook.viewer;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.viewer.widget.ViewerWidget;

import java.util.List;

/**
 * Created on 17-Mar-17.
 */

class ViewerAdapter extends PagerAdapter {

    private final Context context;
    private final List<Card> cards;
    private final ViewerCallback callback;

    ViewerAdapter(ViewerActivity activity, List<Card> cards) {
        this.context = activity;
        this.cards = cards;
        this.callback = activity;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        ViewerWidget widget = new ViewerWidget(context, callback);
        widget.setData(cards.get(position));
        widget.setTag(position);

        container.addView(widget);

        return widget;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
