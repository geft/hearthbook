package com.lifeof843.hearthbook.viewer.audio;

import android.support.annotation.NonNull;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 05-Apr-17.
 */

@Parcel
public class ViewerAudioViewModel extends CoreViewModel {

    @NonNull
    ViewerAudioData data = new ViewerAudioData();
}
