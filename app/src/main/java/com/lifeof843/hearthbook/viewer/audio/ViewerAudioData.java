package com.lifeof843.hearthbook.viewer.audio;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.viewer.ViewerCallback;

import org.parceler.Parcel;
import org.parceler.Transient;

/**
 * Created on 05-Apr-17.
 */

@Parcel
public class ViewerAudioData {

    @NonNull
    public String cardId = "";

    @Nullable
    @Transient
    public ViewerCallback callback;
}
