package com.lifeof843.hearthbook.viewer;

import android.databinding.ObservableBoolean;
import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.core.CoreViewModel;
import com.lifeof843.hearthbook.viewer.audio.ViewerAudioDialogData;

import org.parceler.Parcel;

/**
 * Created on 17-Mar-17.
 */

@Parcel
public class ViewerViewModel extends CoreViewModel {

    public ObservableBoolean isUnlocked = new ObservableBoolean();

    @Nullable
    Integer currPosition;

    @Nullable
    ViewerAudioDialogData audioDialogData;
}
