package com.lifeof843.hearthbook.viewer.simple;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetViewerSimpleBinding;

/**
 * Created on 17-Mar-17.
 */

public class ViewerSimpleWidget extends CoreWidget<ViewerSimpleData, WidgetViewerSimpleBinding, ViewerSimpleViewModel> {

    public ViewerSimpleWidget(@NonNull Context context) {
        super(context);
    }

    public ViewerSimpleWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_viewer_simple;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected ViewerSimpleViewModel createViewModel() {
        return new ViewerSimpleViewModel();
    }

    @Override
    public void setData(ViewerSimpleData data) {
        viewModel.label.set(data.label);
        viewModel.content.set(data.content);
    }

    @Nullable
    @Override
    public ViewerSimpleData getData() {
        ViewerSimpleData data = new ViewerSimpleData();
        data.label = viewModel.label.get();
        data.content = viewModel.content.get();

        return data;
    }

}
