package com.lifeof843.hearthbook.credits;

import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;

import com.lifeof843.hearthbook.Henson;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.CardSet;
import com.lifeof843.hearthbook.constant.SortType;
import com.lifeof843.hearthbook.core.GalleryActivity;
import com.lifeof843.hearthbook.data.CardIdData;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.filterable.FilterableProcessor;
import com.lifeof843.hearthbook.filterable.type.FilterSet;
import com.lifeof843.hearthbook.loader.AssetLoader;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.util.ObservableUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created on 16-Apr-17.
 */

public class CreditsActivity extends GalleryActivity<Card, CreditsViewModel> {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gallery;
    }

    @Override
    protected CreditsViewModel createViewModel() {
        return new CreditsViewModel();
    }

    @Override
    protected List<Card> getGalleryData() throws IOException {
        return new AssetLoader(this).getAllCards();
    }

    @Override
    protected void handleList(List<Card> items) {
        List<Filterable> filterableList = new ArrayList<>();
        filterableList.add(new FilterSet(CardSet.CREDITS));

        disposable.add(
                new FilterableProcessor(this, filterableList).applyFiltersFromList(items)
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(cards -> {
                            viewModel.cards = cards;
                            updateListContent(cards);
                        }, this::mapError)
        );
    }

    @Override
    protected void updateListContent(List<Card> items) {
        disposable.add(
                Observable.fromIterable(items)
                        .map(card -> card.getName())
                        .toList()
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(list -> {
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
                            adapter.addAll(list);

                            binding.listView.setAdapter(adapter);
                            binding.listView.setOnItemClickListener((parent, view, position, id) ->
                                    handleItemClick(items, position));
                        })
        );
    }

    private void handleItemClick(List<Card> items, int position) {
        disposable.add(
                Observable.fromIterable(items)
                        .map(card -> card.getId())
                        .toList()
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(ids -> {
                            CardIdData.setCardIdList(ids);

                            startActivity(
                                Henson.with(this).gotoViewerActivity()
                                        .isEntourage(false)
                                        .position(position)
                                        .sortType(SortType.NAME)
                                        .build()
                            );
                        }, this::mapError)
        );
    }

    @Nullable
    @Override
    protected List<Card> getItems() {
        return viewModel.cards;
    }

    @Override
    protected boolean isItemValid(CharSequence query, Card item) {
        String lowerCaseQuery = query.toString().toLowerCase();

        return (item.getName() != null && item.getName().toLowerCase().contains(lowerCaseQuery)) ||
                (item.getText() != null && item.getText().toLowerCase().contains(lowerCaseQuery));
    }

}
