package com.lifeof843.hearthbook.credits;

import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.core.CoreViewModel;
import com.lifeof843.hearthbook.model.Card;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created on 16-Apr-17.
 */

@Parcel
class CreditsViewModel extends CoreViewModel {

    @Nullable
    List<Card> cards;
}
