package com.lifeof843.hearthbook.util;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.lifeof843.hearthbook.BuildConfig;

/**
 * Created on 19-Mar-17.
 */

public class ExceptionUtil {

    private static final String TAG = "ExceptionUtil";

    public static void log(String message) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, message);
        } else {
            Crashlytics.log(message);
        }
    }

    public static void log(Throwable e) {
        if (BuildConfig.DEBUG) {
            e.printStackTrace();
        } else {
            Crashlytics.logException(e);
        }
    }

    public static void log(Throwable e, String message) {
        log(message);
        log(e);
    }
}
