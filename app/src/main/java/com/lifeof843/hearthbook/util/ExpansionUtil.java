package com.lifeof843.hearthbook.util;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import com.google.android.vending.expansion.APKExpansionHelper;
import com.google.android.vending.expansion.zipfile.APKExpansionSupport;
import com.lifeof843.hearthbook.HearthbookApplication;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.expansion.ExpDownloaderActivity;

import io.reactivex.Single;

/**
 * Created on 21-Apr-17.
 */

public class ExpansionUtil {

    public static void validateExpansion(Context context, ExpansionCallback callback) {
        FileUtil.clearObbFiles(context.getApplicationContext());

        if (doesExpansionFileExist(context)) {
            ProgressDialog dialog = new ProgressDialog(context);
            dialog.setIndeterminate(true);
            dialog.setMessage(context.getString(R.string.dialog_loading));
            dialog.show();

            Single.fromCallable(() -> initAssets(context))
                    .compose(ObservableUtil.applyCommonSchedulers())
                    .subscribe(result -> {
                        dialog.dismiss();
                        callback.onLoaded();
                    }, ExceptionUtil::log);
        } else {
            initExpansionDownload(context);
        }
    }

    public static boolean initAssets(Context context) {
        try {
            HearthbookApplication.zipResourceFile = APKExpansionSupport.getAPKExpansionZipFile(context.getApplicationContext());
            HearthbookApplication.zipEntries = APKExpansionSupport.getZipEntries(context.getApplicationContext());
            return true;
        } catch (Exception e) {
            ExceptionUtil.log(e, context.getString(R.string.error_load_zip));
            return false;
        }
    }

    private static void initExpansionDownload(Context context) {
        PendingIntent pendingIntent = createPendingIntent(context);
        new APKExpansionHelper(context, pendingIntent);
    }

    private static boolean doesExpansionFileExist(Context context) {
        String[] expansionFiles = APKExpansionSupport.getAPKExpansionFiles(
                context, HearthbookApplication.EXT_VERSION_MAIN, 0);
        return expansionFiles.length == 1;
    }

    private static PendingIntent createPendingIntent(Context context) {
        Intent notifierIntent = new Intent(context, ExpDownloaderActivity.class);
        notifierIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return PendingIntent.getActivity(
                context.getApplicationContext(), 0, notifierIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public interface ExpansionCallback {
        void onLoaded();
    }
}
