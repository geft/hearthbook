package com.lifeof843.hearthbook.util;

import android.support.annotation.Nullable;

/**
 * Created on 23-Mar-17.
 */

public class StringUtil {

    @Nullable
    public static String capitalizeFirst(String input) {
        if (input == null || input.length() < 1) return "";

        return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
    }

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }
}
