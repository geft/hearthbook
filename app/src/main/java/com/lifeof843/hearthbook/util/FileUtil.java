package com.lifeof843.hearthbook.util;

import android.content.Context;
import android.os.Environment;

import java.io.File;

import static com.lifeof843.hearthbook.HearthbookApplication.EXT_VERSION_MAIN;

/**
 * Created on 02-Apr-17.
 */

@SuppressWarnings("WeakerAccess")
public class FileUtil {

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void clearObbFiles(Context context) {
        String path = Environment.getExternalStorageDirectory().getPath()
                + "/Android/obb/"
                + context.getPackageName();

        String expPath = path
                + "/main."
                + EXT_VERSION_MAIN
                + "."
                + context.getPackageName()
                + ".obb";

        File dir = new File(path);

        if (dir.exists()) {
            File[] files = dir.listFiles();

            if (files != null) for (File file : files)
                if (!file.getPath().equalsIgnoreCase(expPath)) {
                    file.delete();
                }
        }
    }
}
