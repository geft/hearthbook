package com.lifeof843.hearthbook.util;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.CardSet;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.filterable.type.FilterSet;
import com.lifeof843.hearthbook.model.Card;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 15-Apr-17.
 */

public class FilterableUtil {

    @SuppressWarnings("Convert2streamapi")
    public static List<Filterable> getStandardFilters() {
        List<Filterable> list = new ArrayList<>();

        for (CardSet cardSet : CardSet.getStandard()) {
            list.add(new FilterSet(cardSet));
        }

        return mergeList(list);
    }

    public static List<Filterable> mergeList(List<Filterable> list) {
        List<Filterable> newList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {

            Filterable filterable = list.get(i);

            while (i < list.size() - 1 && list.get(i).getType() == list.get(i + 1).getType()) {
                filterable = merge(filterable, list.get(i + 1));
                i++;
            }

            newList.add(filterable);
        }

        return newList;
    }

    public static Filterable merge(Filterable f1, Filterable f2) {
        return new Filterable() {
            @Override
            public boolean isValid(Card card) {
                return f1.isValid(card) || f2.isValid(card);
            }

            @Override
            public CardFilter getType() {
                return f1.getType();
            }
        };
    }
}
