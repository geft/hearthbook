package com.lifeof843.hearthbook.util;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;
import android.util.Pair;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.lifeof843.hearthbook.constant.Duration;

import java.util.ArrayList;
import java.util.List;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION_CODES;

/**
 * Created on 19-Mar-17.
 */

@SuppressWarnings("unused")
public class AnimUtil {

    public static void revealCircular(View view) {
        if (view != null && VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            int cx = view.getWidth() / 2;
            int cy = view.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
            view.setVisibility(View.VISIBLE);
            anim.start();
        }
    }

    public static void hideCircular(View view) {
        if (view != null && VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            int cx = view.getWidth() / 2;
            int cy = view.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);
            Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);
            view.setVisibility(View.INVISIBLE);
            anim.start();
        }
    }

    public static void animateRotate180(View view, boolean isRotated) {
        int angle = isRotated ? 180 : 0;
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "rotation",angle, angle + 180);
        anim.setDuration(Duration.SHORT);
        anim.start();
    }

    public static void hideKeyboard(Context context) {
        InputMethodManager imm =
                (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    public static Bundle getTransitionBundleToViewer(View itemView, @StringRes int sharedNameRes, @Nullable List<Pair<View, String>> morePairs) {
        String elementName = itemView.getContext().getString(sharedNameRes);
        itemView.setTransitionName(elementName);

        List<Pair<View, String>> pairs = getSharedPair(itemView.getContext());
        pairs.add(new Pair<>(itemView, elementName));

        if (morePairs != null) {
            pairs.addAll(morePairs);
        }

        return getBundleFromPair(itemView.getContext(), pairs);
    }

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    private static List<Pair<View, String>> getSharedPair(Context context) {
        View statusBar = ((Activity) context).findViewById(android.R.id.statusBarBackground);
        View navigationBar = ((Activity) context).findViewById(android.R.id.navigationBarBackground);

        List<Pair<View, String>> pairs = new ArrayList<>();

        if (statusBar != null) {
            pairs.add(Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME));
        }

        if (navigationBar != null) {
            pairs.add(Pair.create(navigationBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME));
        }

        return pairs;
    }

    @SuppressWarnings("unchecked")
    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    private static Bundle getBundleFromPair(Context context, List<Pair<View, String>> pairs) {
        try {
            return ActivityOptions.makeSceneTransitionAnimation(
                    (Activity) context, pairs.toArray(new Pair[pairs.size()])).toBundle();
        } catch (IllegalArgumentException e) {
            ExceptionUtil.log(e);
            return new Bundle();
        }
    }
}
