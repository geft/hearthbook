package com.lifeof843.hearthbook.util;

import android.Manifest;
import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;

/**
 * Created on 30-Mar-17.
 */

public class PermissionUtil {

    public static boolean isStoragePermissionGranted(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return permissionCheck == PermissionChecker.PERMISSION_GRANTED;
    }
}
