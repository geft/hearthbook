package com.lifeof843.hearthbook.util;

import android.content.Context;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.lifeof843.hearthbook.BuildConfig;
import com.lifeof843.hearthbook.R;

/**
 * Created on 21-Apr-17.
 */

public class AdUtil {

    public static void initAds(Context context) {
        MobileAds.initialize(
                context.getApplicationContext(),
                context.getString(R.string.banner_ad_app_id));
    }

    public static AdRequest getAdRequest() {
        AdRequest adRequest;

        if (BuildConfig.DEBUG) {
            adRequest = new AdRequest.Builder().addTestDevice("D53A82A58022CF71D289B1FEE3B9C6DF").build();
        } else {
            adRequest = new AdRequest.Builder()
                    .addKeyword("game")
                    .addKeyword("board game")
                    .addKeyword("tcg")
                    .addKeyword("card")
                    .addKeyword("online")
                    .addKeyword("blizzard")
                    .addKeyword("hearthstone")
                    .build();
        }

        return adRequest;
    }
}
