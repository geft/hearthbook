package com.lifeof843.hearthbook.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.lifeof843.hearthbook.constant.Duration;

/**
 * Created on 03-Apr-17.
 */

public class SnackBarUtil {

    public static void show(Context context, @StringRes int stringId) {
        showDefault(context, stringId, SnackBarType.DEFAULT);
    }


    @SuppressWarnings("SameParameterValue")
    public static void show(Context context, @StringRes int stringId, SnackBarType type) {
        showDefault(context, stringId, type);
    }

    private static void showDefault(Context context, @StringRes int stringId, SnackBarType type) {
        View view;

        try {
            view = ((Activity) context).findViewById(android.R.id.content);
        } catch (ClassCastException | NullPointerException e) {
            view = null;
        }

        if (view != null) {
            Snackbar snackbar = Snackbar.make(view, stringId, Duration.VERY_LONG);
            snackbar.getView().setBackgroundColor(getColorFromType(type));
            snackbar.show();
        }
    }

    private static int getColorFromType(SnackBarType type) {
        switch (type) {
            case ERROR: return Color.RED;
            default: return Color.BLACK;
        }
    }

    public enum SnackBarType {
        DEFAULT, ERROR
    }
}
