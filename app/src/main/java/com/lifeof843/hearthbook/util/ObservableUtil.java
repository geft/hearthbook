package com.lifeof843.hearthbook.util;

import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created on 18-Mar-17.
 */

public class ObservableUtil {

    public static <R> SingleTransformer<R, R> applyCommonSchedulers() {
        return observable -> observable.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
