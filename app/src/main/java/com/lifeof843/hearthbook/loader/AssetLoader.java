package com.lifeof843.hearthbook.loader;

import android.content.Context;

import com.bluelinelabs.logansquare.LoganSquare;
import com.lifeof843.hearthbook.constant.CardLanguage;
import com.lifeof843.hearthbook.constant.ZipPath;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.model.CardBack;
import com.lifeof843.hearthbook.model.CardEntourage;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created on 14-Mar-17.
 */

public class AssetLoader {

    private static final String JSON_CARDS = "cards.json";
    private static final String JSON_CARD_BACKS = "cardbacks.json";
    private static final String JSON_ENTOURAGE = "entourage.json";

    private final Context context;

    public AssetLoader(Context context) {
        this.context = context;
    }

    public List<Card> getAllCards() throws IOException {
        CardLanguage language = new PrefLoader(context).loadLanguage();
        InputStream inputStream;

        if (language == CardLanguage.ENGLISH) {
            inputStream = getDefaultCardJson();
        } else {
            String path = ZipPath.JSON.getPath() + language.getId() + ZipPath.JSON.getExt();
            inputStream = new ExpLoader(context).getStreamFromPath(path);
        }

        if (inputStream == null) inputStream = getDefaultCardJson();

        return LoganSquare.parseList(inputStream, Card.class);
    }

    private InputStream getDefaultCardJson() throws IOException {
        return context.getAssets().open(JSON_CARDS);
    }

    public List<CardBack> getAllCardBacks() throws IOException {
        InputStream inputStream = context.getAssets().open(JSON_CARD_BACKS);
        return LoganSquare.parseList(inputStream, CardBack.class);
    }

    public List<CardEntourage> getEntourage() throws IOException {
        InputStream inputStream = context.getAssets().open(JSON_ENTOURAGE);
        return LoganSquare.parseList(inputStream, CardEntourage.class);
    }
}
