package com.lifeof843.hearthbook.loader;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;

import com.google.android.vending.expansion.zipfile.ZipResourceFile;
import com.lifeof843.hearthbook.HearthbookApplication;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.ZipPath;
import com.lifeof843.hearthbook.model.CardSound;
import com.lifeof843.hearthbook.util.ExceptionUtil;
import com.lifeof843.hearthbook.util.SnackBarUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created on 29-Mar-17.
 */

public class ExpLoader {

    private final Context context;

    @Nullable
    private final ZipResourceFile zipResourceFile = HearthbookApplication.zipResourceFile;

    @Nullable
    private final TreeSet<String> zipEntries = HearthbookApplication.zipEntries;

    public ExpLoader(Context context) {
        this.context = context;
    }

    @Nullable
    public InputStream getStreamFromPath(String path) {
        if (zipResourceFile == null) return null;

        try {
            return zipResourceFile.getInputStream(path);
        } catch (IOException e) {
            ExceptionUtil.log(e, context.getString(R.string.error_load_path_format, path));
            return null;
        }
    }

    @Nullable
    public Drawable getDrawableFromPath(String path) {
        return getDrawableFromStream(getStreamFromPath(path));
    }

    private Drawable getDrawableFromStream(InputStream inputStream) {
        try {
            return Drawable.createFromStream(inputStream, null);
        } catch (OutOfMemoryError e) {
            SnackBarUtil.show(context, R.string.error_out_of_memory);
        }

        return new ColorDrawable(Color.TRANSPARENT);
    }

    public List<CardSound> getAudioList(String cardId) {
        if (zipEntries == null) return new ArrayList<>();

        List<CardSound> list = new ArrayList<>();
        String audioPath = ZipPath.AUDIO.getPath() + cardId;
        SortedSet<String> sortedSet = zipEntries.subSet(audioPath, audioPath + "\uffff");

        if (sortedSet.size() > 0) {
            for (String path : sortedSet) {
                CardSound cardSound = createSound(path);
                if (cardSound != null) list.add(cardSound);
            }
        }

        return list;
    }

    @Nullable
    private CardSound createSound(String path) {
        if (zipResourceFile == null) return null;

        AssetFileDescriptor descriptor = zipResourceFile.getAssetFileDescriptor(path);

        return descriptor != null ? new CardSound(path, descriptor) : null;
    }
}
