package com.lifeof843.hearthbook.loader;

import android.content.Context;
import android.content.SharedPreferences;

import com.lifeof843.hearthbook.constant.CardLanguage;

/**
 * Created on 15-Apr-17.
 */

public class PrefLoader {

    private static final String PREF_COMMON = "PREF_COMMON";
    private static final String LANGUAGE = "LANGUAGE";
    private static final String UNLOCK = "UNLOCK";

    private final Context context;

    public PrefLoader(Context context) {
        this.context = context;
    }

    private SharedPreferences getPref() {
        return context.getSharedPreferences(PREF_COMMON, Context.MODE_PRIVATE);
    }

    public void saveLanguage(CardLanguage language) {
        getPref().edit()
                .putString(LANGUAGE, language.getId())
                .apply();
    }

    public CardLanguage loadLanguage() {
        String languageId = getPref().getString(LANGUAGE, "");

        for (CardLanguage language : CardLanguage.values()) {
            if (language.getId().equalsIgnoreCase(languageId)) {
                return language;
            }
        }

        return CardLanguage.ENGLISH;
    }

    public void saveUnlock() {
        getPref().edit()
                .putBoolean(UNLOCK, true)
                .apply();
    }

    public boolean isUnlocked() {
        return getPref().getBoolean(UNLOCK, false);
    }
}
