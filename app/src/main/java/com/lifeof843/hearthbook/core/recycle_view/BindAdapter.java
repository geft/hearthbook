package com.lifeof843.hearthbook.core.recycle_view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.lifeof843.hearthbook.BR;

import java.util.List;

/**
 * Created on 14-Mar-17.
 */

public abstract class BindAdapter<T> extends RecyclerView.Adapter<BindViewHolder> implements OnItemClickListener<T> {

    private final LayoutInflater inflater;

    @LayoutRes
    private final int itemLayout;

    private final List<T> items;

    protected BindAdapter(Context context, @LayoutRes int itemLayout, List<T> items) {
        this.inflater = LayoutInflater.from(context);
        this.itemLayout = itemLayout;
        this.items = items;
    }

    @Override
    public BindViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(inflater, itemLayout, parent, false);

        return new BindViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindViewHolder holder, int position) {
        ViewDataBinding binding = holder.getBinding();
        T item = items.get(position);

        binding.getRoot().setOnClickListener(v -> onItemClick(item, position));
        binding.setVariable(BR.item, item);
        binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
