package com.lifeof843.hearthbook.core.widget.accordion;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetAccordionBinding;
import com.lifeof843.hearthbook.util.AnimUtil;

/**
 * Created on 19-Mar-17.
 */

public class AccordionWidget
        extends CoreWidget<AccordionWidgetData, WidgetAccordionBinding, AccordionWidgetViewModel>
        implements View.OnClickListener {

    public AccordionWidget(@NonNull Context context) {
        super(context);
    }

    public AccordionWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_accordion;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);
    }

    @Override
    protected AccordionWidgetViewModel createViewModel() {
        return new AccordionWidgetViewModel();
    }

    @Override
    public void setData(AccordionWidgetData data) {
        viewModel.label.set(data.label);
        binding.content.addView(data.view);
    }

    @Nullable
    @Override
    public AccordionWidgetData getData() {
        AccordionWidgetData data = new AccordionWidgetData();
        data.label = viewModel.label.get();
        data.view = binding.content.getChildAt(0);

        return data;
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.header)) {
            AnimUtil.animateRotate180(binding.imageArrow, viewModel.isExpanded.get());
            viewModel.isExpanded.set(!viewModel.isExpanded.get());
        }
    }
}
