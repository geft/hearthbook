package com.lifeof843.hearthbook.core.widget.accordion;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 19-Mar-17.
 */

@Parcel
public class AccordionWidgetViewModel extends CoreViewModel {

    public ObservableBoolean isExpanded = new ObservableBoolean();

    public ObservableField<String> label = new ObservableField<>();
}
