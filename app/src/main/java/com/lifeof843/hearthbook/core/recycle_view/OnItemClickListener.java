package com.lifeof843.hearthbook.core.recycle_view;

/**
 * Created on 17-Mar-17.
 */

public interface OnItemClickListener<T> {

    void onItemClick(T item, int position);
}
