package com.lifeof843.hearthbook.core.widget.accordion;

import android.view.View;


/**
 * Created on 19-Mar-17.
 */

public class AccordionWidgetData {

    public String label;

    public View view;
}
