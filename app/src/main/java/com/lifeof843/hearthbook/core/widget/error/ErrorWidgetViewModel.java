package com.lifeof843.hearthbook.core.widget.error;

import android.databinding.ObservableField;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 18-Mar-17.
 */

@Parcel
public class ErrorWidgetViewModel extends CoreViewModel {

    public ObservableField<String> errorText = new ObservableField<>();
}
