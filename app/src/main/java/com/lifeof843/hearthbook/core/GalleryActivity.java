package com.lifeof843.hearthbook.core;

import android.app.SearchManager;
import android.content.Context;
import android.support.annotation.Nullable;

import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView;
import com.lifeof843.hearthbook.constant.Duration;
import com.lifeof843.hearthbook.databinding.ActivityGalleryBinding;
import com.lifeof843.hearthbook.util.ObservableUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created on 16-Apr-17.
 */

public abstract class GalleryActivity<T, VM extends CoreViewModel> extends CoreActivity<ActivityGalleryBinding, VM> {

    @Override
    protected void initView() {
        initToolbar();
        initList();
        initSearch();
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initList() {
        disposable.add(
                Single.fromCallable(this::getGalleryData)
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(this::handleList, this::mapError)
        );
    }

    protected abstract List<T> getGalleryData() throws IOException;

    protected abstract void handleList(List<T> items);

    protected abstract void updateListContent(List<T> items);

    @Nullable
    protected abstract List<T> getItems();

    protected abstract boolean isItemValid(CharSequence query, T item);

    private void initSearch() {
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        binding.search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        binding.search.setIconifiedByDefault(false);
        binding.search.setQuery("", false);

        disposable.add(
                RxSearchView.queryTextChanges(binding.search)
                        .skipInitialValue()
                        .debounce(Duration.SHORT, TimeUnit.MILLISECONDS, Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::performFilter, this::mapError)
        );
    }

    private void performFilter(CharSequence query) {
        if (query.length() == 0) {
            updateListContent(getItems());
        } else if (getItems() != null) {
            disposable.add(
                    Observable.fromIterable(getItems())
                            .filter((item) -> isItemValid(query, item))
                            .toList()
                            .compose(ObservableUtil.applyCommonSchedulers())
                            .subscribe(this::updateListContent, this::mapError)
            );
        }
    }
}
