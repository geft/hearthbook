package com.lifeof843.hearthbook.core.recycle_view;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;

/**
 * Created on 14-Mar-17.
 */

public class BindViewHolder extends RecyclerView.ViewHolder {

    private final ViewDataBinding binding;

    BindViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());

        this.binding = binding;
    }

    public ViewDataBinding getBinding() {
        return binding;
    }
}
