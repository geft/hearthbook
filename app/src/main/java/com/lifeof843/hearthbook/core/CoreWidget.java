package com.lifeof843.hearthbook.core;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.lifeof843.hearthbook.BR;
import com.lifeof843.hearthbook.util.ExceptionUtil;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created on 15-Mar-17.
 */

@SuppressWarnings("unused")
public abstract class CoreWidget<T, B extends ViewDataBinding, VM extends CoreViewModel> extends FrameLayout {

    protected CompositeDisposable disposable = new CompositeDisposable();

    protected B binding = DataBindingUtil.inflate(
            LayoutInflater.from(getContext()), getLayout(), this, true);

    protected final VM viewModel = createViewModel();

    public CoreWidget(@NonNull Context context) {
        super(context);
        bindView();
        initView();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public CoreWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        bindView();
        initView();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @LayoutRes
    protected abstract int getLayout();

    private void bindView() {
        binding.setVariable(BR.viewModel, viewModel);
    }

    protected abstract void initView();

    protected abstract VM createViewModel();

    public abstract void setData(T data);

    @Nullable
    public abstract T getData();

    protected View getView() {
        return binding.getRoot();
    }

    public void mapError(Throwable e) {
        ExceptionUtil.log(e);
    }

    @Override
    protected void onDetachedFromWindow() {
        if (disposable != null) disposable.dispose();

        super.onDetachedFromWindow();
    }
}
