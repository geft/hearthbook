package com.lifeof843.hearthbook.core.item;

import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.support.annotation.DrawableRes;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;
import org.parceler.Parcels;
import org.parceler.converter.NullableParcelConverter;

/**
 * Created on 20-Mar-17.
 */

@Parcel
public class CheckBoxItem<T> extends BaseObservable {

    @ParcelPropertyConverter(ParcelsWrapperConverter.class)
    public T id;

    @DrawableRes
    public int icon;

    public String label;

    public ObservableBoolean isChecked = new ObservableBoolean();

    public static class ParcelsWrapperConverter extends NullableParcelConverter<Object> {

        @Override
        public void nullSafeToParcel(Object input, android.os.Parcel parcel) {
            parcel.writeParcelable(Parcels.wrap(input), 0);
        }

        @Override
        public Object nullSafeFromParcel(android.os.Parcel parcel) {
            return Parcels.unwrap(parcel.readParcelable(ParcelsWrapperConverter.class.getClassLoader()));
        }
    }
}
