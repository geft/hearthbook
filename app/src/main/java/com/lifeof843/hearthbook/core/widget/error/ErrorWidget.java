package com.lifeof843.hearthbook.core.widget.error;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetErrorBinding;

/**
 * Created on 18-Mar-17.
 */

public class ErrorWidget extends CoreWidget<String, WidgetErrorBinding, ErrorWidgetViewModel> {

    public ErrorWidget(@NonNull Context context) {
        super(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_error;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected ErrorWidgetViewModel createViewModel() {
        return new ErrorWidgetViewModel();
    }

    @Override
    public void setData(String data) {
        viewModel.errorText.set(data);
    }

    @Nullable
    @Override
    public String getData() {
        return viewModel.errorText.get();
    }

}
