package com.lifeof843.hearthbook.core.view;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Created on 20-Mar-17.
 */

public class CoreButton extends AppCompatButton {

    private Disposable disposable;

    public CoreButton(Context context) {
        super(context);
    }

    public CoreButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CoreButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDelayedClickListener(CoreButtonCallback callback) {
        disposable = RxView.clicks(this)
                .throttleFirst(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> callback.onClick());
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (disposable != null) disposable.dispose();
    }

    public interface CoreButtonCallback {
        void onClick();
    }
}
