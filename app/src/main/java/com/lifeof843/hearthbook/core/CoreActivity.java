package com.lifeof843.hearthbook.core;

import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.f2prateek.dart.Dart;
import com.lifeof843.hearthbook.BR;
import com.lifeof843.hearthbook.BuildConfig;
import com.lifeof843.hearthbook.core.widget.error.ErrorWidget;
import com.lifeof843.hearthbook.util.ExceptionUtil;

import org.parceler.ParcelerRuntimeException;
import org.parceler.Parcels;

import io.fabric.sdk.android.Fabric;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created on 13-Mar-17.
 */

public abstract class CoreActivity<B extends ViewDataBinding, VM extends CoreViewModel> extends AppCompatActivity {

    private static final String PARCEL_KEY = "viewModel";
    private boolean isOrientationChanged;

    protected final CompositeDisposable disposable = new CompositeDisposable();
    protected B binding;
    protected VM viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initTransition();
        initViewModel(savedInstanceState);
        initFabric();
        initDart();
        initBinding();
        initView();
    }

    private void initTransition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(new Explode());
            getWindow().setExitTransition(new Explode());
        }
    }


    private void initViewModel(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            try {
                viewModel = Parcels.unwrap(savedInstanceState.getParcelable(PARCEL_KEY));
            } catch (ParcelerRuntimeException e) {
                ExceptionUtil.log(e);
                viewModel = createViewModel();
            }
        } else {
            viewModel = createViewModel();
        }
    }

    private void initFabric() {
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
    }

    private void initDart() {
        Dart.inject(this);
    }

    private void initBinding() {
        binding = DataBindingUtil.setContentView(this, getLayoutId());
        binding.setVariable(BR.viewModel, viewModel);
    }

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract void initView();

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(PARCEL_KEY, Parcels.wrap(viewModel));

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();

        super.onDestroy();
    }

    protected abstract VM createViewModel();

    @SuppressWarnings("WeakerAccess")
    public void mapError(Throwable e) {
        ExceptionUtil.log(e);

        ErrorWidget widget = new ErrorWidget(this);
        widget.setData(e.getMessage());
        addContentView(widget, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    protected void navigateTo(Class<?> cls) {
        startActivity(new Intent(this, cls));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        isOrientationChanged = !isOrientationChanged;
    }

    @Override
    public void supportFinishAfterTransition() {
        if (isOrientationChanged) {
            finish();
        } else {
            super.supportFinishAfterTransition();
        }
    }
}
