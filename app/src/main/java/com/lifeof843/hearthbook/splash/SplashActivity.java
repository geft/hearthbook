package com.lifeof843.hearthbook.splash;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.lifeof843.hearthbook.BuildConfig;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreActivity;
import com.lifeof843.hearthbook.databinding.ActivitySplashBinding;
import com.lifeof843.hearthbook.home.HomeActivity;
import com.lifeof843.hearthbook.util.AdUtil;
import com.lifeof843.hearthbook.util.ExceptionUtil;
import com.lifeof843.hearthbook.util.ExpansionUtil;
import com.lifeof843.hearthbook.util.ObservableUtil;

import java.util.Calendar;

import io.fabric.sdk.android.Fabric;
import io.reactivex.Single;

/**
 * Created on 16-Mar-17.
 */

public class SplashActivity extends CoreActivity<ActivitySplashBinding, SplashViewModel> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        disposable.add(
                Single.fromCallable(() -> {
                    initUncaught();
                    initFabric();
                    ExpansionUtil.initAssets(this);
                    AdUtil.initAds(this);

                    return true;
                })
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(result -> navigateTo(HomeActivity.class))
        );
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        viewModel.year.set(Calendar.getInstance().get(Calendar.YEAR));
    }

    @Override
    protected SplashViewModel createViewModel() {
        return new SplashViewModel();
    }

    private void initUncaught() {
        Thread.setDefaultUncaughtExceptionHandler ((thread, e) -> {
            ExceptionUtil.log(e);

            if (!BuildConfig.DEBUG) {
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);

                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, pendingIntent);
                System.exit(2);
            }
        });
    }

    private void initFabric() {
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(getApplicationContext(), crashlyticsKit, new Answers());
    }
}
