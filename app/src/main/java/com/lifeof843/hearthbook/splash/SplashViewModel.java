package com.lifeof843.hearthbook.splash;

import android.databinding.ObservableInt;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 07-Apr-17.
 */

@Parcel
public class SplashViewModel extends CoreViewModel {

    public ObservableInt year = new ObservableInt();
}
