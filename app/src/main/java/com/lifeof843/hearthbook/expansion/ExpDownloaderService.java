/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lifeof843.hearthbook.expansion;

import com.google.android.vending.expansion.downloader.impl.DownloaderService;

/**
 * This class demonstrates the minimal client implementation of the
 * DownloaderService from the Downloader library.
 */
public class ExpDownloaderService extends DownloaderService {

    @SuppressWarnings("SpellCheckingInspection")
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtH4R1GGHfzX8cOl+owzFgR0S+ODRyAVjOpR1Kj4Q0WuoL3KdirWgWVYvzQk+qZV74kEfbYt0dLQk+64KxUttEyQokQWC+vMWkYeqIlFHDXQykMZryIB1kBcsbLVQ4H29wUBsvC3kT3qoA2KnpJ1jpN2MQAxkOHZWL6hso2lsFEnkvLTrYOejPaV8u32CLtGTR7DjW3TSWTqpGAP/SdHnISgnWWzVyD8e0fI3kYEPv7x6kQKfItwQ4j70FrNxvvdLUj8AKB4xQlCzIhe49QsIR4mCOJ/iQJFcHiqw0DyWWihLjCDhbHcM2qwbhf9cCcTZ/Ntneak//1e/54PJ0MOvmQIDAQAB";

    private static final byte[] SALT = new byte[] {
            1, 43, -12, -1, 54, 98,
            -100, -12, 43, 2, -8, -4, 9, 5, -106, -108, -33, 45, -1, 84
    };

    /**
     * This public key comes from your Android Market publisher account, and it
     * used by the LVL to validate responses from Market on your behalf.
     */
    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    /**
     * This is used by the preference obfuscator to make sure that your
     * obfuscated preferences are different than the ones used by other
     * applications.
     */
    @Override
    public byte[] getSALT() {
        return SALT.clone();
    }

    /**
     * Fill this in with the class name for your alarm receiver. We do this
     * because receivers must be unique across all of Android (it's a good idea
     * to make sure that your receiver is in your unique package)
     */
    @Override
    public String getAlarmReceiverClassName() {
        return ExpAlarmReceiver.class.getName();
    }

}
