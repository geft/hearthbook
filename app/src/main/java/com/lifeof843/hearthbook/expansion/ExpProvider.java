package com.lifeof843.hearthbook.expansion;

import com.google.android.vending.expansion.zipfile.APEZProvider;

/**
 * Created on 30-Mar-17.
 */

public class ExpProvider extends APEZProvider {

    @Override
    public String getAuthority() {
        return "com.lifeof843.hearthbook.expansion.ExpProvider";
    }
}
