package com.lifeof843.hearthbook.home;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;
import com.lifeof843.hearthbook.HearthbookApplication;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.browser.BrowserActivity;
import com.lifeof843.hearthbook.card_back.CardBackActivity;
import com.lifeof843.hearthbook.core.CoreActivity;
import com.lifeof843.hearthbook.credits.CreditsActivity;
import com.lifeof843.hearthbook.databinding.ActivityHomeBinding;
import com.lifeof843.hearthbook.setting.SettingActivity;
import com.lifeof843.hearthbook.util.ExpansionUtil;
import com.lifeof843.hearthbook.util.PermissionUtil;
import com.lifeof843.hearthbook.util.SnackBarUtil;

import org.codechimp.apprater.AppRater;

/**
 * Created on 02-Apr-17.
 */

public class HomeActivity extends CoreActivity<ActivityHomeBinding, HomeViewModel>
        implements View.OnClickListener {

    private static final int PERMISSION_STORAGE = 100;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initView() {
        binding.setOnClick(this);

        initStoragePermission();
        initInstabug();
        initAppRater();
    }

    @Override
    protected HomeViewModel createViewModel() {
        return new HomeViewModel();
    }

    private void initStoragePermission() {
        if (PermissionUtil.isStoragePermissionGranted(this)) {
            if (!HearthbookApplication.isAssetInitialized()) {
                validateExpansion();
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE);
        }
    }

    private void validateExpansion() {
        ExpansionUtil.validateExpansion(this, () -> {});
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PermissionChecker.PERMISSION_GRANTED) {
                validateExpansion();
            } else {
                SnackBarUtil.show(this, R.string.home_permission_denied, SnackBarUtil.SnackBarType.ERROR);
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.browser_dialog_quit_message)
                .setPositiveButton(R.string.common_ok, (dialog, which) -> super.onBackPressed())
                .setNeutralButton(R.string.common_cancel, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    private void initInstabug() {
        new Instabug.Builder(getApplication(), "47a87f7d649cec41a668bc0ff24c00d9")
                .setInvocationEvent(InstabugInvocationEvent.SHAKE)
                .setCommentFieldRequired(true)
                .setEmailFieldRequired(true)
                .build();
    }

    private void initAppRater() {
        AppRater.app_launched(this);
        AppRater.setDontRemindButtonVisible(false);
        AppRater.setNumLaunchesForRemindLater(7);
        AppRater.setCancelable(false);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(binding.buttonBrowser))
            navigateTo(BrowserActivity.class);
//        else if (v.equals(binding.buttonDeck))
//            navigateTo(DeckManagerActivity.class);
        else if (v.equals(binding.buttonCardBacks))
            navigateTo(CardBackActivity.class);
        else if (v.equals(binding.buttonCredits))
            navigateTo(CreditsActivity.class);
        else if (v.equals(binding.buttonSetting))
            navigateTo(SettingActivity.class);
    }
}
