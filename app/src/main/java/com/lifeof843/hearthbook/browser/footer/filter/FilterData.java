package com.lifeof843.hearthbook.browser.footer.filter;

import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.filterable.widget.check_box.FilterCheckBoxData;
import com.lifeof843.hearthbook.filterable.widget.range.FilterRangeData;

/**
 * Created on 20-Mar-17.
 */

public class FilterData {

    public FilterCheckBoxData setData = new FilterCheckBoxData();

    public FilterCheckBoxData rarityData = new FilterCheckBoxData();

    public FilterCheckBoxData classData = new FilterCheckBoxData();

    public FilterCheckBoxData gangData = new FilterCheckBoxData();

    public FilterCheckBoxData raceData = new FilterCheckBoxData();

    public FilterCheckBoxData typeData = new FilterCheckBoxData();

    public FilterRangeData costData = new FilterRangeData();

    public FilterRangeData attackData = new FilterRangeData();

    public FilterRangeData healthData = new FilterRangeData();

    @Nullable
    public FilterCallback callback;
}
