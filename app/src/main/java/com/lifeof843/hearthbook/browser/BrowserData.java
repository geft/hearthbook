package com.lifeof843.hearthbook.browser;

import android.content.ComponentName;
import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.constant.SortType;
import com.lifeof843.hearthbook.model.Card;

import org.parceler.Transient;

import java.util.List;

/**
 * Created on 15-Mar-17.
 */

class BrowserData {

    boolean isIncludeText;
    boolean isStandardOnly;
    boolean isCollectibleOnly;

    @Nullable
    ComponentName componentName;

    @Nullable
    List<Card> cards;
    @Transient
    SortType sortType = SortType.NAME;
}
