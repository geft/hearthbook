package com.lifeof843.hearthbook.browser.footer;

import com.lifeof843.hearthbook.browser.footer.filter.FilterData;
import com.lifeof843.hearthbook.constant.StatRange;
import com.lifeof843.hearthbook.core.item.CheckBoxItem;
import com.lifeof843.hearthbook.filterable.widget.check_box.FilterCheckBoxData;
import com.lifeof843.hearthbook.filterable.widget.range.FilterRangeData;
import com.lifeof843.hearthbook.model.CardListItem;
import com.lifeof843.hearthbook.model.CardLister;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 21-Mar-17.
 */

public class BrowserFooterGenerator {

    private BrowserCallback browserCallback;
    private boolean isStandardOnly;

    public BrowserFooterData getFooterData(BrowserCallback callback, boolean isStandardOnly) {
        this.browserCallback = callback;
        this.isStandardOnly = isStandardOnly;

        BrowserFooterData data = new BrowserFooterData();
        data.filterData = getFilterData();
        data.callback = callback;

        return data;
    }

    private FilterData getFilterData() {
        CardLister lister = new CardLister();
        FilterData data = new FilterData();

        data.setData =  new FilterCheckBoxData(getList(lister.getSetList(isStandardOnly)));
        data.rarityData = new FilterCheckBoxData(getList(lister.getRarityList()));
        data.classData = new FilterCheckBoxData(getList(lister.getClassList()));
        data.gangData = new FilterCheckBoxData(getList(lister.getGangList()));
        data.raceData = new FilterCheckBoxData(getList(lister.getRaceList()));
        data.typeData = new FilterCheckBoxData(getList(lister.getTypeList()));

        data.costData = getRangeData(StatRange.COST);
        data.attackData = getRangeData(StatRange.ATTACK);
        data.healthData = getRangeData(StatRange.HEALTH);

        data.callback = filterData -> browserCallback.performFilter(filterData);

        return data;
    }

    @SuppressWarnings("Convert2streamapi")
    private List<CheckBoxItem> getList(List<CardListItem> items) {
        List<CheckBoxItem> list = new ArrayList<>();

        for (CardListItem item : items) {
            list.add(getCheckBoxData(item));
        }

        return list;
    }

    private CheckBoxItem getCheckBoxData(CardListItem item) {
        CheckBoxItem data = new CheckBoxItem();
        data.id = item.getId();
        data.icon = item.getIcon();
        data.label = item.getLabel();
        return data;
    }

    private FilterRangeData getRangeData(StatRange range) {
        FilterRangeData data = new FilterRangeData();
        data.startValue = range.getMin();
        data.endValue = range.getMax();
        data.selectedStart = range.getMin();
        data.selectedEnd = range.getMax();
        return data;
    }
}
