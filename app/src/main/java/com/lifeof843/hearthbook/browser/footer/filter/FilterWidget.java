package com.lifeof843.hearthbook.browser.footer.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.View;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.core.item.CheckBoxItem;
import com.lifeof843.hearthbook.core.widget.accordion.AccordionWidget;
import com.lifeof843.hearthbook.core.widget.accordion.AccordionWidgetData;
import com.lifeof843.hearthbook.databinding.WidgetFilterBinding;
import com.lifeof843.hearthbook.filterable.widget.check_box.FilterCheckBoxData;
import com.lifeof843.hearthbook.filterable.widget.check_box.FilterCheckBoxWidget;
import com.lifeof843.hearthbook.filterable.widget.range.FilterRangeData;
import com.lifeof843.hearthbook.filterable.widget.range.FilterRangeWidget;

import java.util.List;

/**
 * Created on 19-Mar-17.
 */

public class FilterWidget extends CoreWidget<FilterData, WidgetFilterBinding, FilterViewModel> {

    public FilterWidget(@NonNull Context context) {
        super(context);
    }

    public FilterWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_filter;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected FilterViewModel createViewModel() {
        return new FilterViewModel();
    }

    @Override
    public void setData(FilterData data) {
        viewModel.data = data;

        addCheckBoxItemsToLayout(R.string.filter_set, data.setData);
        addCheckBoxItemsToLayout(R.string.filter_rarity, data.rarityData);
        addCheckBoxItemsToLayout(R.string.filter_class, data.classData);
        addCheckBoxItemsToLayout(R.string.filter_gangs, data.gangData);
        addCheckBoxItemsToLayout(R.string.filter_race, data.raceData);
        addCheckBoxItemsToLayout(R.string.filter_type, data.typeData);

        addRangeBarToLayout(R.string.filter_cost, data.costData);
        addRangeBarToLayout(R.string.filter_attack, data.attackData);
        addRangeBarToLayout(R.string.filter_health, data.healthData);
    }

    private void addCheckBoxItemsToLayout(@StringRes int label, FilterCheckBoxData data) {
        FilterCheckBoxWidget widget = getFilterCheckBoxWidget(data.getCheckBoxItems());
        widget.setData(data);

        binding.container.addView(getAccordion(label, widget));
    }

    private void addRangeBarToLayout(@StringRes int label, FilterRangeData data) {
        FilterRangeWidget widget = new FilterRangeWidget(getContext());
        widget.setData(data);

        binding.container.addView(getAccordion(label, widget));
    }

    @NonNull
    private FilterCheckBoxWidget getFilterCheckBoxWidget(final List<CheckBoxItem> items) {
        return new FilterCheckBoxWidget(getContext()) {
                @Override
                protected List<CheckBoxItem> getCheckBoxItems() {
                    return items;
                }
            };
    }

    private View getAccordion(@StringRes int label, View view) {
        AccordionWidgetData data = new AccordionWidgetData();
        data.label = getContext().getString(label);
        data.view = view;

        AccordionWidget widget = new AccordionWidget(getContext());
        widget.setData(data);

        return widget;
    }

    @Nullable
    @Override
    public FilterData getData() {
        return viewModel.data;
    }

}
