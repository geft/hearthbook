package com.lifeof843.hearthbook.browser;

import com.lifeof843.hearthbook.constant.SortType;
import com.lifeof843.hearthbook.model.Card;

import java.util.Collections;
import java.util.List;

/**
 * Created on 24-Mar-17.
 */

public class SortProcessor {

    private List<Card> cards;

    public List<Card> sort(List<Card> cards) {
        this.cards = cards;

        return sortByName();
    }

    public List<Card> sort(SortType sortType, List<Card> cards) {
        this.cards = cards;

        switch (sortType) {
            case ID:
                return sortById();
            case NAME:
                return sortByName();
            case COST:
                return sortByCost();
            case ATTACK:
                return sortByAttack();
            case HEALTH:
                return sortByHealth();
            default:
                return sortByName();
        }
    }

    private List<Card> sortById() {
        Collections.sort(cards, (o1, o2) -> {
            if (o1.getId() != null && o2.getId() != null) {
                return o1.getId().compareTo(o2.getId());
            } else if (o1.getId() == null) {
                return 1;
            } else {
                return 0;
            }
        });

        return cards;
    }

    private List<Card> sortByName() {
        Collections.sort(cards, (o1, o2) -> {
                    if (o1.getName() != null && o2.getName() != null) {
                        return o1.getName().compareTo(o2.getName());
                    } else if (o1.getName() == null) {
                        return 1;
                    } else return 0;
                });

        return cards;
    }

    private List<Card> sortByCost() {
        sortById();

        Collections.sort(cards, (o1, o2) -> o1.getCost() - o2.getCost());

        return cards;
    }

    private List<Card> sortByAttack() {
        sortById();

        Collections.sort(cards, (o1, o2) -> o1.getAttack() - o2.getAttack());

        return cards;
    }

    private List<Card> sortByHealth() {
        sortById();

        Collections.sort(cards, (o1, o2) -> {
                    int health1 = o1.getHealth() == 0 ? o1.getDurability() : o1.getHealth();
                    int health2 = o2.getHealth() == 0 ? o2.getDurability() : o2.getHealth();

                    return health1 - health2;
                });

        return cards;
    }
}
