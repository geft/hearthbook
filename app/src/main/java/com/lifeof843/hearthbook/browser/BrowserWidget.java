package com.lifeof843.hearthbook.browser;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;

import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView;
import com.lifeof843.hearthbook.Henson;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.Duration;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.core.recycle_view.BindAdapter;
import com.lifeof843.hearthbook.data.CardIdData;
import com.lifeof843.hearthbook.databinding.WidgetBrowserBinding;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.filterable.FilterableProcessor;
import com.lifeof843.hearthbook.filterable.type.FilterCollectible;
import com.lifeof843.hearthbook.filterable.type.FilterDefault;
import com.lifeof843.hearthbook.filterable.type.FilterName;
import com.lifeof843.hearthbook.filterable.type.FilterText;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.util.AnimUtil;
import com.lifeof843.hearthbook.util.FilterableUtil;
import com.lifeof843.hearthbook.util.ObservableUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created on 15-Mar-17.
 */

public class BrowserWidget extends CoreWidget<BrowserData, WidgetBrowserBinding, BrowserWidgetViewModel> {

    public BrowserWidget(@NonNull Context context) {
        super(context);
    }

    public BrowserWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_browser;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected BrowserWidgetViewModel createViewModel() {
        return new BrowserWidgetViewModel();
    }

    @Override
    public void setData(BrowserData data) {
        viewModel.data = data;

        initList(data.cards);
        initSearch();
    }

    @Nullable
    @Override
    public BrowserData getData() {
        return getBrowserData();
    }

    public Toolbar getToolbar() {
        return binding.toolbar;
    }

    private BrowserData getBrowserData() {
        return viewModel.data;
    }

    private void initList(@Nullable List<Card> cardList) {
        if (cardList == null) {
            disposable.add(
                    new FilterableProcessor(getContext(), getDefaultFilters())
                            .applyFiltersFromAsset()
                            .compose(ObservableUtil.applyCommonSchedulers())
                            .subscribe(this::handleCards, this::mapError)
            );
        } else {
            handleCards(cardList);
        }
    }

    private void handleCards(List<Card> cards) {
        getBrowserData().cards = cards;
        initRecyclerView(cards);
    }

    private void initRecyclerView(List<Card> cards) {
        BindAdapter<Card> adapter = getBindAdapter(cards);

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.setHasFixedSize(true);
    }

    private void initSearch() {
        SearchManager searchManager =
                (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);

        if (searchManager != null) {
            binding.search.setSearchableInfo(searchManager.getSearchableInfo(getBrowserData().componentName));
        }

        binding.search.setIconifiedByDefault(false);
        binding.search.setQuery("", false);

        disposable.add(
                RxSearchView.queryTextChanges(binding.search)
                        .skipInitialValue()
                        .debounce(Duration.SHORT, TimeUnit.MILLISECONDS, Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::performFilter, this::mapError)
        );
    }

    private void performFilter(CharSequence query) {
        if (query.length() == 0) {
            updateList(getBrowserData().cards);
            return;
        }

        List<Filterable> filterableList = getDefaultFilters();

        if (getBrowserData().isIncludeText) {
            filterableList.add(FilterableUtil.merge(
                    new FilterName(query.toString()), new FilterText(query.toString())));
        } else {
            filterableList.add(new FilterName(query.toString()));
        }

        disposable.add(
                new FilterableProcessor(getContext(), filterableList).applyFiltersFromList(getBrowserData().cards)
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(this::updateList, this::mapError)
        );
    }

    private List<Filterable> getDefaultFilters() {
        List<Filterable> list = new ArrayList<>();
        list.add(new FilterDefault());

        if (getBrowserData().isStandardOnly) {
            list.addAll(FilterableUtil.getStandardFilters());
        }

        if (getBrowserData().isCollectibleOnly) {
            list.add(new FilterCollectible(true));
        }

        return list;
    }

    private void updateList(List<Card> cards) {
        binding.recyclerView.setAdapter(getBindAdapter(cards));
    }

    @NonNull
    private BindAdapter<Card> getBindAdapter(List<Card> cards) {
        return new BindAdapter<Card>(getContext(), R.layout.widget_browser_item, cards) {
            @Override
            public void onItemClick(Card item, int position) {
                extractNamesFromCards(position, cards);
            }
        };
    }

    private void extractNamesFromCards(int position, List<Card> cards) {
        Observable.fromIterable(cards)
                .flatMap(card -> card.getId() != null ? Observable.just(card.getId()) : Observable.empty())
                .toList()
                .compose(ObservableUtil.applyCommonSchedulers())
                .subscribe(cardIds -> goToViewer(position, cardIds)
        );
    }

    private void goToViewer(int position, List<String> cardIds) {
        AnimUtil.hideKeyboard(getContext());

        getContext().startActivity(getGoToViewerIntent(position, cardIds));

        // todo anim
//        Single.fromCallable(() -> {
//            Intent intent = getGoToViewerIntent(position, cardIds);
//            View itemView = binding.recyclerView.findViewHolderForAdapterPosition(position).itemView;
//
//            return new Pair<>(intent, itemView);
//        })
//                .compose(ObservableUtil.applyCommonSchedulers())
//                .subscribe(intentViewPair -> {
//                    Intent intent = intentViewPair.first;
//                    View view = intentViewPair.second;
//
//                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                        Bundle bundle = AnimUtil.getTransitionBundleToViewer(view, R.string.transition_card_name, null);
//                        getContext().startActivity(intent, bundle);
//                    } else {
//                        getContext().startActivity(intent);
//                    }
//                }, ExceptionUtil::log);
    }

    private Intent getGoToViewerIntent(int position, List<String> cardIds) {
        CardIdData.setCardIdList(cardIds);

        return Henson.with(getContext())
                    .gotoViewerActivity()
                    .isEntourage(false)
                    .position(position)
                    .sortType(viewModel.data.sortType)
                    .build();
    }
}
