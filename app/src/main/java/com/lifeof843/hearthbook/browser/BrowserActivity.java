package com.lifeof843.hearthbook.browser;

import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.browser.footer.BrowserCallback;
import com.lifeof843.hearthbook.browser.footer.BrowserFooterData;
import com.lifeof843.hearthbook.browser.footer.BrowserFooterGenerator;
import com.lifeof843.hearthbook.browser.footer.filter.FilterData;
import com.lifeof843.hearthbook.constant.SortType;
import com.lifeof843.hearthbook.core.CoreActivity;
import com.lifeof843.hearthbook.databinding.ActivityBrowserBinding;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.filterable.FilterableConverter;
import com.lifeof843.hearthbook.filterable.FilterableProcessor;
import com.lifeof843.hearthbook.loader.PrefLoader;
import com.lifeof843.hearthbook.util.ObservableUtil;
import com.lifeof843.hearthbook.util.SnackBarUtil;

import java.util.List;

/**
 * Created on 13-Mar-17.
 */

public class BrowserActivity
        extends CoreActivity<ActivityBrowserBinding, BrowserViewModel>
        implements BrowserCallback {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_browser;
    }

    @Override
    protected void initView() {
        viewModel.setUnlocked(new PrefLoader(this).isUnlocked());

        binding.widgetBrowser.setData(getDefaultBrowserData());
        binding.widgetFooter.setData(getDefaultFooterData());

        initToolbar();
    }

    private void initToolbar() {
        Toolbar toolbar = binding.widgetBrowser.getToolbar();
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.browser, menu);

        MenuItem menuText = menu.findItem(R.id.menu_search_text);
        if (viewModel.isUnlocked()) {
            menuText.setChecked(viewModel.isIncludeText());
        } else {
            menuText.setCheckable(false);
        }

        MenuItem menuStandard = menu.findItem(R.id.menu_standard_only);
        if (viewModel.isUnlocked()) {
            menuStandard.setChecked(viewModel.isStandardOnly());
        } else {
            menuStandard.setCheckable(false);
        }

        MenuItem menuCollectible = menu.findItem(R.id.menu_collectible_only);
        if (viewModel.isUnlocked()) {
            menuCollectible.setChecked(viewModel.isCollectibleOnly());
        } else {
            menuCollectible.setCheckable(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item.setChecked(!item.isChecked());

        switch (item.getItemId()) {
            case R.id.menu_search_text:
                if (viewModel.isUnlocked()) {
                    viewModel.setIncludeText(item.isChecked());
                    performReset();
                } else {
                    SnackBarUtil.show(this, R.string.setting_unlock_required);
                }
                return true;
            case R.id.menu_standard_only:
                if (viewModel.isUnlocked()) {
                    viewModel.setStandardOnly(item.isChecked());
                    performReset();
                } else {
                    SnackBarUtil.show(this, R.string.setting_unlock_required);
                }
                return true;
            case R.id.menu_collectible_only:
                if (viewModel.isUnlocked()) {
                    viewModel.setCollectibleOnly(item.isChecked());
                    performReset();
                } else {
                    SnackBarUtil.show(this, R.string.setting_unlock_required);
                }
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private BrowserFooterData getDefaultFooterData() {
        return new BrowserFooterGenerator().getFooterData(this, viewModel.isStandardOnly());
    }

    @Override
    protected BrowserViewModel createViewModel() {
        return new BrowserViewModel();
    }

    private BrowserData getDefaultBrowserData() {
        BrowserData data = new BrowserData();
        data.isIncludeText = viewModel.isIncludeText();
        data.isStandardOnly = viewModel.isStandardOnly();
        data.isCollectibleOnly = viewModel.isCollectibleOnly();
        data.componentName = getComponentName();

        return data;
    }

    @Override
    public void performSort(SortType sortType) {
        BrowserData browserData = binding.widgetBrowser.getData();

        if (browserData != null) {
            browserData.sortType = sortType;
            new SortProcessor().sort(sortType, browserData.cards);
            binding.widgetBrowser.setData(browserData);
        }
    }

    @Override
    public void performFilter(FilterData filterData) {
        BrowserData browserData = binding.widgetBrowser.getData();

        if (browserData != null) {
            List<Filterable> filterableList = new FilterableConverter(filterData).getFilterableList();

            disposable.add(
                    new FilterableProcessor(this, filterableList).applyFiltersFromAsset()
                            .compose(ObservableUtil.applyCommonSchedulers())
                            .subscribe(cards -> {
                                cards = new SortProcessor().sort(browserData.sortType, cards);
                                browserData.cards = cards;
                                binding.widgetBrowser.setData(browserData);
                            })
            );
        }
    }

    @Override
    public void performReset() {
        binding.widgetBrowser.setData(getDefaultBrowserData());
        binding.widgetFooter.setData(getDefaultFooterData());
    }
}
