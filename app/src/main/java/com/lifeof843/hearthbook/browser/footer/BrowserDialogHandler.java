package com.lifeof843.hearthbook.browser.footer;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.browser.footer.filter.FilterData;
import com.lifeof843.hearthbook.browser.footer.filter.FilterWidget;
import com.lifeof843.hearthbook.constant.SortType;
import com.lifeof843.hearthbook.util.StringUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created on 24-Mar-17.
 */

class BrowserDialogHandler {

    private final Context context;

    BrowserDialogHandler(Context context) {
        this.context = context;
    }

    void showSortDialog(BrowserCallback callback) {
        AppCompatDialog sortDialog = new AppCompatDialog(context);

        List<SortType> sortTypes = new ArrayList<>();
        Collections.addAll(sortTypes, SortType.values());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1);
        for (SortType sortType : sortTypes) {
            adapter.add(StringUtil.capitalizeFirst(sortType.name()));
        }

        ListView listView = new ListView(context);
        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            callback.performSort(sortTypes.get(position));
            sortDialog.dismiss();
        });

        sortDialog.setContentView(listView);
        sortDialog.show();
    }

    AlertDialog getFilterDialog(FilterData filterData) {
        FilterWidget widget = new FilterWidget(context);
        widget.setData(filterData);

        AlertDialog filterDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.browser_footer_filter)
                .setView(widget)
                .setPositiveButton(R.string.common_ok, (dialog, which) -> {
                    if (filterData != null && filterData.callback != null)
                        filterData.callback.filter(filterData);
                })
                .setNeutralButton(R.string.common_cancel, (dialog, which) -> dialog.dismiss())
                .create();

        if (filterDialog.getWindow() != null) {
            filterDialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
        }

        return filterDialog;
    }
}
