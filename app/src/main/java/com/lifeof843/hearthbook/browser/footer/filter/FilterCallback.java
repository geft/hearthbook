package com.lifeof843.hearthbook.browser.footer.filter;

/**
 * Created on 24-Mar-17.
 */

public interface FilterCallback {

    void filter(FilterData filterData);
}
