package com.lifeof843.hearthbook.browser.footer;

import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.browser.footer.filter.FilterData;

/**
 * Created on 20-Mar-17.
 */

public class BrowserFooterData {

    @Nullable
    FilterData filterData;

    @Nullable
    BrowserCallback callback;
}
