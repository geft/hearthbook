package com.lifeof843.hearthbook.browser.footer;

import com.lifeof843.hearthbook.browser.footer.filter.FilterData;
import com.lifeof843.hearthbook.constant.SortType;

/**
 * Created on 24-Mar-17.
 */

public interface BrowserCallback {

    void performSort(SortType sortType);

    void performFilter(FilterData filterData);

    void performReset();
}
