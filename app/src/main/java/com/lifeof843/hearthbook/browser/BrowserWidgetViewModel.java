package com.lifeof843.hearthbook.browser;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;
import org.parceler.Transient;

/**
 * Created on 15-Mar-17.
 */

@Parcel
public class BrowserWidgetViewModel extends CoreViewModel {

    @Transient
    BrowserData data;
}
