package com.lifeof843.hearthbook.browser.footer.filter;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;
import org.parceler.Transient;

/**
 * Created on 19-Mar-17.
 */

@Parcel
public class FilterViewModel extends CoreViewModel {

    @Transient
    FilterData data;
}
