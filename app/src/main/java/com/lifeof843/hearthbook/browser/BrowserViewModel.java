package com.lifeof843.hearthbook.browser;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 17-Mar-17.
 */

@Parcel
public class BrowserViewModel extends CoreViewModel {

    boolean isUnlocked;
    boolean isIncludeText;
    boolean isStandardOnly;
    boolean isCollectibleOnly;

    public boolean isUnlocked() {
        return isUnlocked;
    }

    void setUnlocked(boolean unlocked) {
        isUnlocked = unlocked;
    }

    public boolean isIncludeText() {
        return isIncludeText;
    }

    void setIncludeText(boolean includeText) {
        isIncludeText = includeText;
    }

    public boolean isStandardOnly() {
        return isStandardOnly;
    }

    void setStandardOnly(boolean standardOnly) {
        isStandardOnly = standardOnly;
    }

    public boolean isCollectibleOnly() {
        return isCollectibleOnly;
    }

    void setCollectibleOnly(boolean collectibleOnly) {
        isCollectibleOnly = collectibleOnly;
    }
}
