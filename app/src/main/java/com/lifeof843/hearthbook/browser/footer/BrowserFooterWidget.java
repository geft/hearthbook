package com.lifeof843.hearthbook.browser.footer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetBrowserFooterBinding;

/**
 * Created on 20-Mar-17.
 */

public class BrowserFooterWidget
        extends CoreWidget<BrowserFooterData, WidgetBrowserFooterBinding, BrowserFooterViewModel> {

    public BrowserFooterWidget(@NonNull Context context) {
        super(context);
    }

    public BrowserFooterWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_browser_footer;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected BrowserFooterViewModel createViewModel() {
        return new BrowserFooterViewModel();
    }

    @Override
    public void setData(BrowserFooterData data) {
        viewModel.data = data;

        initSort(data);
        initFilter(data);
        initReset(data);
    }

    private void initSort(BrowserFooterData data) {
        binding.buttonSort.setDelayedClickListener(() ->
                new BrowserDialogHandler(getContext()).showSortDialog(data.callback)
        );
    }

    private void initFilter(BrowserFooterData data) {
        binding.buttonFilter.setDelayedClickListener(() ->
                new BrowserDialogHandler(getContext())
                        .getFilterDialog(data.filterData)
                        .show());
    }

    private void initReset(BrowserFooterData data) {
        if (data.callback != null) {
            binding.buttonReset.setDelayedClickListener(() -> data.callback.performReset());
        }
    }

    @Nullable
    @Override
    public BrowserFooterData getData() {
        return viewModel.data;
    }

}
