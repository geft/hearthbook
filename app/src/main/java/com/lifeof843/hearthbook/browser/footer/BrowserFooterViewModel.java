package com.lifeof843.hearthbook.browser.footer;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;
import org.parceler.Transient;

/**
 * Created on 20-Mar-17.
 */

@Parcel
public class BrowserFooterViewModel extends CoreViewModel {

    @Transient
    BrowserFooterData data;
}
