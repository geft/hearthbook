package com.lifeof843.hearthbook;

import android.support.annotation.Nullable;
import android.support.multidex.MultiDexApplication;

import com.google.android.vending.expansion.zipfile.ZipResourceFile;

import java.util.TreeSet;

/**
 * Created on 13-Mar-17.
 */

public class HearthbookApplication extends MultiDexApplication {

    public static final int EXT_VERSION_MAIN = 20;
    public static final long EXT_SIZE_MAIN = 196224849L;

    @Nullable
    public static ZipResourceFile zipResourceFile;

    @Nullable
    public static TreeSet<String> zipEntries;

    public static boolean isAssetInitialized() {
        return zipResourceFile != null && zipEntries != null;
    }
}
