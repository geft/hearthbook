package com.lifeof843.hearthbook.model;

import android.support.annotation.Nullable;

import com.bluelinelabs.logansquare.annotation.JsonObject;

import org.parceler.Parcel;

/**
 * Created on 14-Apr-17.
 */

@Parcel
@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS)
public class CardBack {

    public int id;

    @Nullable
    public String name;

    @Nullable
    public String description;

    @Nullable
    public String source;

    public String getFormattedDescription() {
        if (description == null) return "";
        else return description.replace("\\n", "<br/>");
    }
}
