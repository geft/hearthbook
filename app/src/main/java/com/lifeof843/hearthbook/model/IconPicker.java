package com.lifeof843.hearthbook.model;

import android.support.annotation.DrawableRes;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.CardClass;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 25-Apr-17.
 */

public class IconPicker {

    @DrawableRes
    public static int getClassIcon(CardClass cardClass) {
        return getClassIconMap().get(cardClass);
    }

    private static Map<CardClass, Integer> getClassIconMap() {
        HashMap<CardClass, Integer> map = new HashMap<>();

        map.put(CardClass.DRUID,   R.drawable.ic_class_druid    );
        map.put(CardClass.HUNTER,  R.drawable.ic_class_hunter   );
        map.put(CardClass.MAGE,    R.drawable.ic_class_mage     );
        map.put(CardClass.PALADIN, R.drawable.ic_class_paladin  );
        map.put(CardClass.PRIEST,  R.drawable.ic_class_priest   );
        map.put(CardClass.ROGUE,   R.drawable.ic_class_rogue    );
        map.put(CardClass.SHAMAN,  R.drawable.ic_class_shaman   );
        map.put(CardClass.WARLOCK, R.drawable.ic_class_warlock  );
        map.put(CardClass.WARRIOR, R.drawable.ic_class_warrior  );

        return map;
    }
}
