package com.lifeof843.hearthbook.model;

import android.support.annotation.ColorRes;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.CardClass;
import com.lifeof843.hearthbook.constant.CardRarity;
import com.lifeof843.hearthbook.util.ExceptionUtil;

/**
 * Created on 17-Mar-17.
 */

class ColorPicker {

    @ColorRes
    static int getColorFromRarity(String rarity) {
        if (rarity == null) return R.color.black;

        CardRarity cardRarity;

        try {
            cardRarity = CardRarity.valueOf(rarity);
        } catch (IllegalArgumentException e) {
            ExceptionUtil.log(e);
            return R.color.black;
        }

        switch (cardRarity) {
            case COMMON:
                return R.color.rarity_common;
            case RARE:
                return R.color.rarity_rare;
            case EPIC:
                return R.color.rarity_epic;
            case LEGENDARY:
                return R.color.rarity_legendary;
            default:
                return R.color.black;
        }
    }

    @ColorRes
    static int getColorFromClass(String playerClass) {
        if (playerClass == null) return android.R.color.transparent;

        CardClass cardClass;
        try {
            cardClass = CardClass.valueOf(playerClass);
        } catch (IllegalArgumentException e) {
            ExceptionUtil.log(e);
            return R.color.class_neutral;
        }
        switch (cardClass) {
            case DRUID:
                return R.color.class_druid;
            case HUNTER:
                return R.color.class_hunter;
            case MAGE:
                return R.color.class_mage;
            case PALADIN:
                return R.color.class_paladin;
            case PRIEST:
                return R.color.class_priest;
            case ROGUE:
                return R.color.class_rogue;
            case SHAMAN:
                return R.color.class_shaman;
            case WARLOCK:
                return R.color.class_warlock;
            case WARRIOR:
                return R.color.class_warrior;
            default:
                return R.color.class_neutral;
        }
    }
}
