package com.lifeof843.hearthbook.model;

import com.lifeof843.hearthbook.constant.CardClass;
import com.lifeof843.hearthbook.constant.CardGang;
import com.lifeof843.hearthbook.constant.CardRace;
import com.lifeof843.hearthbook.constant.CardRarity;
import com.lifeof843.hearthbook.constant.CardSet;
import com.lifeof843.hearthbook.constant.CardType;
import com.lifeof843.hearthbook.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16-Mar-17.
 */

public class CardLister {

    public List<CardListItem> getRarityList() {
        List<CardListItem> list = new ArrayList<>();

        for (CardRarity rarity : CardRarity.values()) {
            list.add(new CardListItem<>(
                    rarity.getIconRes(),
                    rarity,
                    StringUtil.capitalizeFirst(rarity.toString())));
        }

        return list;
    }

    public List<CardListItem> getClassList() {
        List<CardListItem> list = new ArrayList<>();

        for (CardClass cardClass : CardClass.getFilterable()) {
            list.add(new CardListItem<>(
                    cardClass.getIconRes(),
                    cardClass,
                    cardClass.getName()
            ));
        }

        return list;
    }

    public List<CardListItem> getSetList(boolean isStandardOnly) {
        List<CardListItem> list = new ArrayList<>();

        for (CardSet set : CardSet.getFilterable()) {
            list.add(new CardListItem<>(
                    set.getIconRes(),
                    set,
                    set.getName()
            ));
        }

        if (isStandardOnly) {
            List<CardSet> standardSets = CardSet.getStandard();
            List<CardListItem> standardList = new ArrayList<>();

            for (CardListItem item : list) {
                CardSet set = (CardSet) item.getId();
                if (standardSets.contains(set)) standardList.add(item);
            }

            return standardList;
        }

        return list;
    }

    public List<CardListItem> getGangList() {
        List<CardListItem> list = new ArrayList<>();

        for (CardGang gang : CardGang.values()) {
            list.add(new CardListItem<>(
                    gang.getIconRes(), gang, gang.getName()
            ));
        }

        return list;
    }

    public List<CardListItem> getRaceList() {
        List<CardListItem> list = new ArrayList<>();

        for (CardRace race : CardRace.values()) {
            list.add(new CardListItem<>(race, StringUtil.capitalizeFirst(race.toString())));
        }

        return list;
    }

    public List<CardListItem> getTypeList() {
        List<CardListItem> list = new ArrayList<>();

        list.add(new CardListItem<>(CardType.ALL, "All Types"));
        list.add(new CardListItem<>(CardType.MINION, "Minion"));
        list.add(new CardListItem<>(CardType.SPELL, "Spell"));
        list.add(new CardListItem<>(CardType.WEAPON, "Weapon"));

        return list;
    }
}
