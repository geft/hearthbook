package com.lifeof843.hearthbook.model;

import android.content.res.AssetFileDescriptor;

/**
 * Created on 30-Mar-17.
 */

public class CardSound {
    private final String path;
    private final AssetFileDescriptor descriptor;

    public CardSound(String name, AssetFileDescriptor descriptor) {
        this.path = name;
        this.descriptor = descriptor;
    }

    public String getPath() {
        return path;
    }

    public AssetFileDescriptor getDescriptor() {
        return descriptor;
    }
}
