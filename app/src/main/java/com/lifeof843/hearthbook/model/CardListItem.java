package com.lifeof843.hearthbook.model;

import android.support.annotation.DrawableRes;

import com.lifeof843.hearthbook.R;

/**
 * Created on 20-Mar-17.
 */

public class CardListItem<T> {

    private final T id;

    @DrawableRes
    private final int icon;

    private final String label;

    public CardListItem(T id, String label) {
        this.icon = R.drawable.ic_empty;
        this.id = id;
        this.label = label;
    }

    public CardListItem(@DrawableRes int icon, T id, String label) {
        this.icon = icon;
        this.id = id;
        this.label = label;
    }

    public int getIcon() {
        return icon;
    }

    public T getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }
}
