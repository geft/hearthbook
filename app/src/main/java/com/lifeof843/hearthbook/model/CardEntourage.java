package com.lifeof843.hearthbook.model;

import com.bluelinelabs.logansquare.annotation.JsonObject;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created on 17-Apr-17.
 */

@Parcel
@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS_AND_ACCESSORS)
public class CardEntourage {

    public String id;

    public List<String> entourage;
}
