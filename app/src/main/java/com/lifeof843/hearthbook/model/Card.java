package com.lifeof843.hearthbook.model;

import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;
import com.lifeof843.hearthbook.constant.CardSet;
import com.lifeof843.hearthbook.constant.CardType;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 14-Mar-17.
 */

@Parcel
@JsonObject(fieldDetectionPolicy = JsonObject.FieldDetectionPolicy.NONPRIVATE_FIELDS_AND_ACCESSORS)
public class Card {

    boolean collectible;

    boolean hideStats;

    int cost;

    int attack;

    int health;

    int durability;

    @Nullable
    String id;

    @Nullable
    String set;

    @Nullable
    String name;

    @Nullable
    String cardClass;

    @Nullable
    List<String> classes;

    @Nullable
    String race;

    @Nullable
    @JsonField(name = "multiClassGroup")
    String gang;

    @Nullable
    String rarity;

    @Nullable
    String artist;

    @Nullable
    String flavor;

    @Nullable
    String type;

    @Nullable
    String text;

    @Nullable
    List<String> entourage;

    public String getText() {
        if (text == null) return "";
        else return text.replace("$", "")
                .replace("\\n", "<br>")
                .replace("[x]", "")
                .replace("#", "")
                .replace("{1} {0}", "")
                .replace("{1}", "")
                .replace("{0}", "");
    }

    @ColorRes
    public int getColorFromRarity() {
        return ColorPicker.getColorFromRarity(getRarity());
    }

    @ColorRes
    public int getColorFromClass() {
        return ColorPicker.getColorFromClass(getCardClass());
    }

    public String getStat() {
        if (getHealth() == 0 && getDurability() == 0) return "";
        else if (isHideStats()) return "";
        else if (getType() != null && getType().equals(CardType.HERO.toString())) return "";
        else {
            return  Integer.toString(this.getAttack()) +
                    "/" +
                    (getHealth() == 0 ? Integer.toString(getDurability()) : getHealth());
        }
    }

    public String getSetLabel() {
        try {
            return CardSet.valueOf(this.set).getName();
        } catch (IllegalArgumentException | NullPointerException e) {
            return "";
        }
    }

    public boolean isCollectible() {
        return collectible;
    }

    public boolean isHideStats() {
        return hideStats;
    }

    public int getCost() {
        return cost;
    }

    public int getAttack() {
        return attack;
    }

    public int getHealth() {
        return health;
    }

    public int getDurability() {
        return durability;
    }

    @Nullable
    public String getId() {
        return id;
    }

    @Nullable
    public String getSet() {
        return set;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getCardClass() {
        return cardClass;
    }

    @Nullable
    public List<String> getClasses() {
        return classes == null ? new ArrayList<>() : new ArrayList<>(classes);
    }

    @Nullable
    public String getRace() {
        return race;
    }

    @Nullable
    public String getGang() {
        return gang;
    }

    @Nullable
    public String getRarity() {
        return rarity;
    }

    @Nullable
    public String getArtist() {
        return artist;
    }

    @Nullable
    public String getFlavor() {
        return flavor;
    }

    @Nullable
    public String getType() {
        return type;
    }

    @Nullable
    public List<String> getEntourage() {
        return entourage == null ? new ArrayList<>() : new ArrayList<>(entourage);
    }

    void setCollectible(boolean collectible) {
        this.collectible = collectible;
    }

    void setHideStats(boolean hideStats) {
        this.hideStats = hideStats;
    }

    void setCost(int cost) {
        this.cost = cost;
    }

    void setAttack(int attack) {
        this.attack = attack;
    }

    void setHealth(int health) {
        this.health = health;
    }

    void setDurability(int durability) {
        this.durability = durability;
    }

    void setId(@Nullable String id) {
        this.id = id;
    }

    void setSet(@Nullable String set) {
        this.set = set;
    }

    void setName(@Nullable String name) {
        this.name = name;
    }

    void setCardClass(@Nullable String cardClass) {
        this.cardClass = cardClass;
    }

    void setClasses(@Nullable List<String> classes) {
        this.classes = classes;
    }

    void setRace(@Nullable String race) {
        this.race = race;
    }

    void setGang(@Nullable String gang) {
        this.gang = gang;
    }

    void setRarity(@Nullable String rarity) {
        this.rarity = rarity;
    }

    void setArtist(@Nullable String artist) {
        this.artist = artist;
    }

    void setFlavor(@Nullable String flavor) {
        this.flavor = flavor;
    }

    void setType(@Nullable String type) {
        this.type = type;
    }

    void setText(@Nullable String text) {
        this.text = text;
    }

    void setEntourage(@Nullable List<String> entourage) {
        this.entourage = entourage;
    }
}
