package com.lifeof843.hearthbook.card_back.widget;

import com.lifeof843.hearthbook.model.CardBack;

import org.parceler.Parcel;

/**
 * Created on 15-Apr-17.
 */

@Parcel
public class CardBackViewerData {

    public CardBack cardBack;
}
