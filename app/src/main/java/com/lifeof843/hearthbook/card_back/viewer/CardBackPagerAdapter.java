package com.lifeof843.hearthbook.card_back.viewer;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.lifeof843.hearthbook.card_back.widget.CardBackViewerData;
import com.lifeof843.hearthbook.card_back.widget.CardBackViewerWidget;
import com.lifeof843.hearthbook.model.CardBack;

import java.util.List;

/**
 * Created on 14-Apr-17.
 */

class CardBackPagerAdapter extends PagerAdapter {

    private final Context context;
    private final List<CardBack> cardBacks;

    CardBackPagerAdapter(Context context, List<CardBack> cardBacks) {
        this.context = context;
        this.cardBacks = cardBacks;
    }

    @Override
    public int getCount() {
        return cardBacks.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        CardBackViewerData data = new CardBackViewerData();
        data.cardBack = cardBacks.get(position);

        CardBackViewerWidget widget = new CardBackViewerWidget(context);
        widget.setData(data);

        container.addView(widget);

        return widget;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
