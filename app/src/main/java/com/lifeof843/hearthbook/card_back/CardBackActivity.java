package com.lifeof843.hearthbook.card_back;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;

import com.lifeof843.hearthbook.Henson;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.GalleryActivity;
import com.lifeof843.hearthbook.loader.AssetLoader;
import com.lifeof843.hearthbook.model.CardBack;
import com.lifeof843.hearthbook.util.ObservableUtil;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created on 31-Mar-17.
 */

public class CardBackActivity extends GalleryActivity<CardBack, CardBackViewModel> {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_gallery;
    }

    @Override
    protected CardBackViewModel createViewModel() {
        return new CardBackViewModel();
    }

    @Override
    protected List<CardBack> getGalleryData() throws IOException {
        return new AssetLoader(this).getAllCardBacks();
    }

    @Override
    protected void handleList(List<CardBack> cardBacks) {
        Collections.sort(cardBacks, (o1, o2) -> {
            if (o1.name == null || o2.name == null) return 0;
            else return o1.name.compareTo(o2.name);
        });

        viewModel.cardBacks = cardBacks;
        updateListContent(cardBacks);
    }

    @Override
    protected void updateListContent(List<CardBack> cardBacks) {
        disposable.add(
                Observable.fromIterable(cardBacks)
                        .map(cardBack -> cardBack.name)
                        .toList()
                        .compose(ObservableUtil.applyCommonSchedulers())
                        .subscribe(list -> {
                            ArrayAdapter<String> adapter = getListAdapter(cardBacks);
                            adapter.addAll(list);

                            binding.listView.setAdapter(adapter);
                            binding.listView.setOnItemClickListener((parent, view, position, id) ->
                                    startActivity(
                                            Henson.with(this).gotoCardBackViewerActivity()
                                                    .cardBacks(cardBacks)
                                                    .position(position)
                                                    .build()
                            ));
                        }, this::mapError)
        );
    }

    @Nullable
    @Override
    protected List<CardBack> getItems() {
        return viewModel.cardBacks;
    }

    @Override
    protected boolean isItemValid(CharSequence query, CardBack item) {
        String lowerCaseQuery = query.toString().toLowerCase();

        return (item.name != null && item.name.toLowerCase().contains(lowerCaseQuery)) ||
                (item.description != null && item.description.toLowerCase().contains(lowerCaseQuery));
    }

    @NonNull
    private ArrayAdapter<String> getListAdapter(final List<CardBack> cardBacks) {
        return new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1) {
                                    @Override
                                    public long getItemId(int position) {
                                        return cardBacks.get(position).id;
                                    }
                                };
    }
}
