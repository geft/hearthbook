package com.lifeof843.hearthbook.card_back.viewer;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.f2prateek.dart.InjectExtra;
import com.google.android.gms.ads.AdListener;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreActivity;
import com.lifeof843.hearthbook.databinding.ActivityCardBackViewerBinding;
import com.lifeof843.hearthbook.loader.PrefLoader;
import com.lifeof843.hearthbook.model.CardBack;
import com.lifeof843.hearthbook.util.AdUtil;

import java.util.List;

/**
 * Created on 14-Apr-17.
 */

public class CardBackViewerActivity extends CoreActivity<ActivityCardBackViewerBinding, CardBackViewerViewModel> {

    @InjectExtra
    int position;

    @InjectExtra
    List<CardBack> cardBacks;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_card_back_viewer;
    }

    @Override
    protected void initView() {
        viewModel.cardBacks = cardBacks;

        initUnlock();
        initPager();
        initButtons();
    }

    @Override
    protected CardBackViewerViewModel createViewModel() {
        return new CardBackViewerViewModel();
    }

    private void initUnlock() {
        boolean isUnlocked = new PrefLoader(this).isUnlocked();
        viewModel.isUnlocked.set(isUnlocked);

        if (!isUnlocked) {
            binding.adView.loadAd(AdUtil.getAdRequest());
            binding.adView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);

                    binding.adView.setVisibility(View.GONE);
                }
            });
        }
    }

    private void initPager() {
        CardBackPagerAdapter adapter = new CardBackPagerAdapter(this, viewModel.cardBacks);

        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setOffscreenPageLimit(2);
        binding.viewPager.setCurrentItem(viewModel.currPosition == null ? position : viewModel.currPosition);
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewModel.currPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initButtons() {
        binding.chevronLeft.setOnClickListener(v ->
                binding.viewPager.setCurrentItem(getCurrentItem() - 1, true));

        binding.chevronRight.setOnClickListener(v ->
                binding.viewPager.setCurrentItem(getCurrentItem() + 1, true));
    }

    private int getCurrentItem() {
        return binding.viewPager.getCurrentItem();
    }
}
