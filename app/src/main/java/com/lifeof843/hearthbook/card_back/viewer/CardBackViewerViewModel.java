package com.lifeof843.hearthbook.card_back.viewer;

import android.databinding.ObservableBoolean;
import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.core.CoreViewModel;
import com.lifeof843.hearthbook.model.CardBack;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created on 14-Apr-17.
 */

@Parcel
public class CardBackViewerViewModel extends CoreViewModel {

    public ObservableBoolean isUnlocked = new ObservableBoolean();

    @Nullable
    Integer currPosition;

    @Nullable
    List<CardBack> cardBacks;
}
