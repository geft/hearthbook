package com.lifeof843.hearthbook.card_back;

import android.support.annotation.Nullable;

import com.lifeof843.hearthbook.core.CoreViewModel;
import com.lifeof843.hearthbook.model.CardBack;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 31-Mar-17.
 */

@Parcel
public class CardBackViewModel extends CoreViewModel {

    @Nullable
    List<CardBack> cardBacks = new ArrayList<>();
}
