package com.lifeof843.hearthbook.card_back.widget;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.constant.Duration;
import com.lifeof843.hearthbook.databinding.WidgetCardBackViewerBinding;
import com.lifeof843.hearthbook.model.CardBack;
import com.lifeof843.hearthbook.util.ExceptionUtil;
import com.lifeof843.hearthbook.util.ObservableUtil;
import com.lifeof843.hearthbook.viewer.image.ViewerImageData;
import com.lifeof843.hearthbook.viewer.image.ViewerImageWidget;
import com.lifeof843.hearthbook.viewer.simple.ViewerSimpleData;
import com.lifeof843.hearthbook.viewer.simple.ViewerSimpleWidget;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created on 15-Apr-17.
 */

class CardBackViewerGenerator {

    private final Context context;
    private final WidgetCardBackViewerBinding binding;

    CardBackViewerGenerator(Context context, WidgetCardBackViewerBinding binding) {
        this.context = context;
        this.binding = binding;
    }

    void init(CardBackViewerData data) {
        CardBack cardBack = data.cardBack;

        Single.zip(
                Single.just(getImageWidget(cardBack)).subscribeOn(Schedulers.computation()),
                Single.just(getViewsFromData(getSimpleData(cardBack))).subscribeOn(Schedulers.computation()),
                (image, simple) -> {
                    List<View> views = new ArrayList<>();
                    views.add(image);
                    views.addAll(simple);
                    views.add(getPadding());

                    return views;
                }
        )
                .compose(ObservableUtil.applyCommonSchedulers())
                .subscribe(this::addViews, ExceptionUtil::log);
    }

    @SuppressWarnings("Convert2streamapi")
    @NonNull
    private List<View> getViewsFromData(List<ViewerSimpleData> dataList) {
        List<View> viewList = new ArrayList<>();
        for (ViewerSimpleData data : dataList) {
            if (data.label != null && data.content != null && !data.content.isEmpty()) {
                ViewerSimpleWidget widget = new ViewerSimpleWidget(context);
                widget.setData(data);
                viewList.add(widget);
            }
        }
        return viewList;
    }

    @NonNull
    private List<ViewerSimpleData> getSimpleData(CardBack cardBack) {
        List<ViewerSimpleData> list = new ArrayList<>();
        list.add(getSimpleData(R.string.card_back_name, cardBack.name));
        list.add(getSimpleData(R.string.card_back_description, cardBack.getFormattedDescription()));

        return list;
    }

    private View getPadding() {
        View view = new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, (int) context.getResources().getDimension(R.dimen.padding_huge)
        ));
        return view;
    }

    @NonNull
    private ViewerImageWidget getImageWidget(CardBack cardBack) {
        ViewerImageData data = new ViewerImageData();
        data.setCardBackId(cardBack.id);

        ViewerImageWidget widget = new ViewerImageWidget(context);
        widget.setData(data);
        return widget;
    }

    private void addViews(List<View> viewList) {
        new Handler().post(() -> {
            for (View view : viewList) {
                getAnimator(view).start();
            }
        });
    }

    private ValueAnimator getAnimator(final View view) {
        ValueAnimator animator = ValueAnimator.ofInt(500, 0);
        animator.setDuration(Duration.SHORT);
        animator.setStartDelay(Duration.SHORT);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setAlpha(0);
                binding.container.addView(view);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setAlpha(1);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                view.setAlpha(1);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.addUpdateListener(animation -> {
            view.setAlpha(animation.getAnimatedFraction());
            view.setTranslationY((int) animation.getAnimatedValue());
        });

        return animator;
    }

    private ViewerSimpleData getSimpleData(@StringRes int label, String content) {
        ViewerSimpleData data = new ViewerSimpleData();
        data.label = context.getString(label);
        data.content = content;

        return data;
    }
}
