package com.lifeof843.hearthbook.card_back.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetCardBackViewerBinding;

/**
 * Created on 15-Apr-17.
 */

public class CardBackViewerWidget extends CoreWidget<CardBackViewerData, WidgetCardBackViewerBinding, CardBackViewerWidgetViewModel>{

    public CardBackViewerWidget(@NonNull Context context) {
        super(context);
    }

    public CardBackViewerWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_card_back_viewer;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected CardBackViewerWidgetViewModel createViewModel() {
        return new CardBackViewerWidgetViewModel();
    }

    @Override
    public void setData(CardBackViewerData data) {
        viewModel.data = data;

        new CardBackViewerGenerator(getContext(), binding).init(data);
    }

    @Nullable
    @Override
    public CardBackViewerData getData() {
        return viewModel.data;
    }

}
