package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.CardRace;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 25-Mar-17.
 */

public class FilterRace implements Filterable {

    private final CardRace cardRace;

    public FilterRace(CardRace cardRace) {
        this.cardRace = cardRace;
    }

    @Override
    public boolean isValid(Card card) {
        return card.getRace() != null && card.getRace().equals(cardRace.toString());
    }

    @Override
    public CardFilter getType() {
        return CardFilter.RACE;
    }
}
