package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 02-Apr-17.
 */

public class FilterText implements Filterable {

    private final String text;

    public FilterText(String text) {
        this.text = text.toLowerCase();
    }

    @Override
    public boolean isValid(Card card) {
        return card.getText().toLowerCase().contains(text);
    }

    @Override
    public CardFilter getType() {
        return CardFilter.TEXT;
    }
}
