package com.lifeof843.hearthbook.filterable.widget.check_box;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.core.item.CheckBoxItem;
import com.lifeof843.hearthbook.databinding.WidgetRecyclerViewBinding;

import java.util.List;

/**
 * Created on 19-Mar-17.
 */

public abstract class FilterCheckBoxWidget extends CoreWidget<FilterCheckBoxData, WidgetRecyclerViewBinding, FilterCheckBoxViewModel> {

    public FilterCheckBoxWidget(@NonNull Context context) {
        super(context);
    }

    public FilterCheckBoxWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_recycler_view;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected FilterCheckBoxViewModel createViewModel() {
        return new FilterCheckBoxViewModel();
    }

    @Override
    public void setData(FilterCheckBoxData data) {
        viewModel.data = data;

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(new FilterCheckBoxListAdapter(getContext(), getCheckBoxItems()));
        binding.recyclerView.setNestedScrollingEnabled(false);
    }

    @Nullable
    @Override
    public FilterCheckBoxData getData() {
        return viewModel.data;
    }

    protected abstract List<CheckBoxItem> getCheckBoxItems();
}
