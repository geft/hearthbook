package com.lifeof843.hearthbook.filterable.widget.range;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.CoreWidget;
import com.lifeof843.hearthbook.databinding.WidgetRangeBarBinding;

/**
 * Created on 22-Mar-17.
 */

public class FilterRangeWidget extends CoreWidget<FilterRangeData, WidgetRangeBarBinding, FilterRangeViewModel> {

    public FilterRangeWidget(@NonNull Context context) {
        super(context);
    }

    public FilterRangeWidget(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int getLayout() {
        return R.layout.widget_range_bar;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected FilterRangeViewModel createViewModel() {
        return new FilterRangeViewModel();
    }

    @Override
    public void setData(FilterRangeData data) {
        viewModel.data = data;

        initRangeBar(data);
    }

    private void initRangeBar(FilterRangeData data) {
        binding.rangeBar.setTickStart(data.startValue);
        binding.rangeBar.setTickEnd(data.endValue);
        binding.rangeBar.setRangePinsByValue(data.selectedStart, data.selectedEnd);
        binding.rangeBar.setFormatter(value -> {
            if (value.equals(Integer.toString(data.endValue))) {
                return value + "+";
            } else return value;
        });
        binding.rangeBar.setOnRangeBarChangeListener(
                (rangeBar, leftPinIndex, rightPinIndex, leftPinValue, rightPinValue) -> {
            data.selectedStart = leftPinIndex;
            data.selectedEnd = rightPinIndex;
        });
    }

    @Nullable
    @Override
    public FilterRangeData getData() {
        return viewModel.data;
    }

}
