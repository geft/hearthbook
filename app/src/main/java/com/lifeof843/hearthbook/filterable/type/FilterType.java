package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.CardType;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 22-Mar-17.
 */

public class FilterType implements Filterable {

    private final CardType cardType;

    public FilterType(CardType cardType) {
        this.cardType = cardType;
    }

    @Override
    public boolean isValid(Card card) {
        return card.getType() != null && card.getType().equals(cardType.toString());
    }

    @Override
    public CardFilter getType() {
        return CardFilter.TYPE;
    }
}
