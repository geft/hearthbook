package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardClass;
import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 25-Mar-17.
 */

public class FilterClass implements Filterable {

    private final CardClass cardClass;

    public FilterClass(CardClass cardClass) {
        this.cardClass = cardClass;
    }

    @Override
    public boolean isValid(Card card) {
        return isClassValid(card) || isMultiClassValid(card);
    }

    private boolean isClassValid(Card card) {
        return card.getCardClass() != null && card.getCardClass().equals(cardClass.toString());
    }

    private boolean isMultiClassValid(Card card) {
        return card.getClasses() != null && card.getClasses().contains(cardClass.toString());
    }

    @Override
    public CardFilter getType() {
        return CardFilter.CLASS;
    }
}
