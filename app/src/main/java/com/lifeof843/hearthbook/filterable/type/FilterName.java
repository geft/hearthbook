package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 22-Mar-17.
 */

public class FilterName implements Filterable {

    private final String name;

    public FilterName(String name) {
        this.name = name.toLowerCase();
    }

    @Override
    public boolean isValid(Card card) {
        return card.getName() != null && card.getName().toLowerCase().contains(name);
    }

    @Override
    public CardFilter getType() {
        return CardFilter.NAME;
    }
}
