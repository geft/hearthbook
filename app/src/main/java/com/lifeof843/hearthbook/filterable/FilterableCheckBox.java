package com.lifeof843.hearthbook.filterable;

import com.lifeof843.hearthbook.core.item.CheckBoxItem;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created on 25-Mar-17.
 */

abstract class FilterableCheckBox<T> {

    List<Filterable> getFilterable(List<CheckBoxItem> items) {
        return getIds(items)
                .map(this::getFilterable)
                .toList()
                .subscribeOn(Schedulers.computation())
                .blockingGet();
    }

    protected abstract Filterable getFilterable(T id);

    @SuppressWarnings("unchecked")
    private Observable<T> getIds(List<CheckBoxItem> items) {
        return Observable.fromIterable(items.subList(1, items.size()))
                .filter(checkBoxItem -> checkBoxItem.isChecked.get())
                .map(item -> (T) item.id);
    }
}
