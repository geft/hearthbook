package com.lifeof843.hearthbook.filterable;

import com.lifeof843.hearthbook.browser.footer.filter.FilterData;
import com.lifeof843.hearthbook.constant.CardClass;
import com.lifeof843.hearthbook.constant.CardGang;
import com.lifeof843.hearthbook.constant.CardRace;
import com.lifeof843.hearthbook.constant.CardRarity;
import com.lifeof843.hearthbook.constant.CardSet;
import com.lifeof843.hearthbook.constant.CardType;
import com.lifeof843.hearthbook.filterable.type.FilterAttack;
import com.lifeof843.hearthbook.filterable.type.FilterClass;
import com.lifeof843.hearthbook.filterable.type.FilterCost;
import com.lifeof843.hearthbook.filterable.type.FilterDefault;
import com.lifeof843.hearthbook.filterable.type.FilterGang;
import com.lifeof843.hearthbook.filterable.type.FilterHealth;
import com.lifeof843.hearthbook.filterable.type.FilterRace;
import com.lifeof843.hearthbook.filterable.type.FilterRarity;
import com.lifeof843.hearthbook.filterable.type.FilterSet;
import com.lifeof843.hearthbook.filterable.type.FilterType;
import com.lifeof843.hearthbook.util.FilterableUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 24-Mar-17.
 */

public class FilterableConverter {

    private final FilterData filterData;

    public FilterableConverter(FilterData filterData) {
        this.filterData = filterData;
    }

    public List<Filterable> getFilterableList() {
        List<Filterable> list  = new ArrayList<>();

        list.add(new FilterDefault());
        list.addAll(getSetFilters());
        list.addAll(getRarityFilters());
        list.addAll(getClassFilters());
        list.addAll(getGangFilters());
        list.addAll(getRaceFilters());
        list.addAll(getTypeFilters());

        if (filterData.costData.isRangeModified()) {
            list.add(new FilterCost(filterData.costData));
        }

        if (filterData.attackData.isRangeModified()) {
            list.add(new FilterAttack(filterData.attackData));
        }

        if (filterData.healthData.isRangeModified()) {
            list.add(new FilterHealth(filterData.healthData));
        }

        return FilterableUtil.mergeList(list);
    }

    private List<Filterable> getSetFilters() {
        return new FilterableCheckBox<CardSet>() {
            @Override
            protected Filterable getFilterable(CardSet id) {
                return new FilterSet(id);
            }
        }.getFilterable(filterData.setData.getCheckBoxItems());
    }

    private List<Filterable> getRarityFilters() {
        return new FilterableCheckBox<CardRarity>() {
            @Override
            protected Filterable getFilterable(CardRarity id) {
                return new FilterRarity(id);
            }
        }.getFilterable(filterData.rarityData.getCheckBoxItems());
    }

    private List<Filterable> getClassFilters() {
        return new FilterableCheckBox<CardClass>() {
            @Override
            protected Filterable getFilterable(CardClass id) {
                return new FilterClass(id);
            }
        }.getFilterable(filterData.classData.getCheckBoxItems());
    }

    private List<Filterable> getGangFilters() {
        return new FilterableCheckBox<CardGang>() {
            @Override
            protected Filterable getFilterable(CardGang id) {
                return new FilterGang(id);
            }
        }.getFilterable(filterData.gangData.getCheckBoxItems());
    }

    private List<Filterable> getRaceFilters() {
        return new FilterableCheckBox<CardRace>() {
            @Override
            protected Filterable getFilterable(CardRace id) {
                return new FilterRace(id);
            }
        }.getFilterable(filterData.raceData.getCheckBoxItems());
    }

    private List<Filterable> getTypeFilters() {
        return new FilterableCheckBox<CardType>() {
            @Override
            protected Filterable getFilterable(CardType id) {
                return new FilterType(id);
            }
        }.getFilterable(filterData.typeData.getCheckBoxItems());
    }
}
