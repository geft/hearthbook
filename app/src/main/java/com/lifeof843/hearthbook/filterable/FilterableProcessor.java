package com.lifeof843.hearthbook.filterable;

import android.content.Context;

import com.lifeof843.hearthbook.browser.SortProcessor;
import com.lifeof843.hearthbook.loader.AssetLoader;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.util.ExceptionUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created on 22-Mar-17.
 */

public class FilterableProcessor {

    private final Context context;
    private final List<Filterable> filterableList;

    public FilterableProcessor(Context context, List<Filterable> filterableList) {
        this.context = context;
        this.filterableList = filterableList;
    }

    public Single<List<Card>> applyFiltersFromAsset() {
        List<Card> cards;

        try {
            cards = new AssetLoader(context).getAllCards();
        } catch (IOException e) {
            ExceptionUtil.log(e);
            return Single.just(new ArrayList<>());
        }

        return applyFiltersFromList(cards);
    }

    public Single<List<Card>> applyFiltersFromList(final List<Card> cards) {
        return Observable.fromIterable(cards)
                .flatMap(item -> Observable.just(item)
                        .filter(card -> {
                            for (int i = 0; i < filterableList.size(); i++) {
                                if (!filterableList.get(i).isValid(card)) return false;
                            }

                            return true;
                        })
                )
                .toList()
                .map(cards1 -> new SortProcessor().sort(cards1));
    }
}
