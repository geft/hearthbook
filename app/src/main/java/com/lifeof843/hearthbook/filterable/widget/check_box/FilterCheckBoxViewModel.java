package com.lifeof843.hearthbook.filterable.widget.check_box;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 19-Mar-17.
 */

@Parcel
public class FilterCheckBoxViewModel extends CoreViewModel {

    FilterCheckBoxData data;
}
