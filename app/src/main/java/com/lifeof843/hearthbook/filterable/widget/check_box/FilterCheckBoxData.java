package com.lifeof843.hearthbook.filterable.widget.check_box;

import com.lifeof843.hearthbook.core.item.CheckBoxItem;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 20-Mar-17.
 */

@Parcel
public class FilterCheckBoxData {

    List<CheckBoxItem> checkBoxItems = new ArrayList<>();

    public FilterCheckBoxData() {}

    public FilterCheckBoxData(List<CheckBoxItem> checkBoxItems) {
        this.checkBoxItems = checkBoxItems;
    }

    public List<CheckBoxItem> getCheckBoxItems() {
        return checkBoxItems;
    }
}
