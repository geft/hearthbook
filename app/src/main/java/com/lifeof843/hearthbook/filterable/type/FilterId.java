package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 22-Mar-17.
 */

public class FilterId implements Filterable {

    private final String id;

    public FilterId(String id) {
        this.id = id;
    }

    @Override
    public boolean isValid(Card card) {
        return card.getId() != null && card.getId().equalsIgnoreCase(id);
    }

    @Override
    public CardFilter getType() {
        return CardFilter.ID;
    }
}
