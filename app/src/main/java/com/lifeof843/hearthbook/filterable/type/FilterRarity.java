package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.CardRarity;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 25-Mar-17.
 */

public class FilterRarity implements Filterable {

    private final CardRarity cardRarity;

    public FilterRarity(CardRarity cardRarity) {
        this.cardRarity = cardRarity;
    }

    @Override
    public boolean isValid(Card card) {
        return card.getRarity() != null && card.getRarity().equals(cardRarity.toString());
    }

    @Override
    public CardFilter getType() {
        return CardFilter.RARITY;
    }
}
