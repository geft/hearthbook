package com.lifeof843.hearthbook.filterable;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 21-Mar-17.
 */

public interface Filterable {

    boolean isValid(Card card);

    CardFilter getType();
}
