package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.StatRange;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.filterable.widget.range.FilterRangeData;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 25-Mar-17.
 */

public class FilterHealth implements Filterable {

    private final int selectedMin;
    private final int selectedMax;

    public FilterHealth(FilterRangeData data) {
        this.selectedMin = data.selectedStart;
        this.selectedMax = data.selectedEnd;
    }

    @Override
    public boolean isValid(Card card) {
        if (card.getHealth() == 0) return card.getDurability() >= selectedMin && isValidMax(card.getDurability());
        else return card.getHealth() >= selectedMin && isValidMax(card.getHealth());
    }

    private boolean isValidMax(int health) {
        return selectedMax == StatRange.HEALTH.getMax() || health <= selectedMax;
    }

    @Override
    public CardFilter getType() {
        return CardFilter.HEALTH;
    }
}
