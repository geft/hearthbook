package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.CardRarity;
import com.lifeof843.hearthbook.constant.CardSet;
import com.lifeof843.hearthbook.constant.CardType;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;
import com.lifeof843.hearthbook.util.StringUtil;

/**
 * Created on 15-Mar-17.
 */

public class FilterDefault implements Filterable {

    @Override
    public boolean isValid(Card card) {
        return isValidType(card) && isValidSet(card) && isValidSpell(card);
    }

    private boolean isValidType(Card card) {
        if (card.getType() == null) return true;

        CardType type = CardType.valueOf(card.getType());

        return  (type != CardType.HERO || isLegendary(card)) &&
                type != CardType.HERO_POWER &&
                type != CardType.ENCHANTMENT;
    }

    private boolean isLegendary(Card card) {
        if (card.getRarity() == null) return false;

        return card.getRarity().equals(CardRarity.LEGENDARY.toString());
    }

    private boolean isValidSpell(Card card) {
        return !(card.getType() != null &&
                card.getType().equals(CardType.SPELL.toString()) &&
                StringUtil.isNullOrEmpty(card.getText()));

    }

    private boolean isValidSet(Card card) {
        if (card.getSet() == null) return true;

        CardSet set = CardSet.valueOf(card.getSet());

        return  set != CardSet.TB &&
                set != CardSet.CREDITS &&
                set != CardSet.HERO_SKINS;
    }

    @Override
    public CardFilter getType() {
        return CardFilter.DEFAULT;
    }
}
