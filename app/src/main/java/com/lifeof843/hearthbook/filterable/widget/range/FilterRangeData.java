package com.lifeof843.hearthbook.filterable.widget.range;

import android.databinding.BaseObservable;

import org.parceler.Parcel;

/**
 * Created on 22-Mar-17.
 */

@Parcel
public class FilterRangeData extends BaseObservable {

    public int startValue;
    public int endValue;
    public int selectedStart = 1;
    public int selectedEnd = 9;

    public boolean isRangeModified() {
        return startValue != selectedStart || endValue != selectedEnd;
    }
}
