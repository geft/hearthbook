package com.lifeof843.hearthbook.filterable.widget.check_box;

import android.content.Context;

import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.core.item.CheckBoxItem;
import com.lifeof843.hearthbook.core.recycle_view.BindAdapter;
import com.lifeof843.hearthbook.core.recycle_view.BindViewHolder;
import com.lifeof843.hearthbook.databinding.WidgetCheckBoxBinding;

import java.util.List;

/**
 * Created on 21-Mar-17.
 */

class FilterCheckBoxListAdapter extends BindAdapter<CheckBoxItem> {

    private final List<CheckBoxItem> items;

    FilterCheckBoxListAdapter(Context context, List<CheckBoxItem> items) {
        super(context, R.layout.widget_check_box, items);

        this.items = items;
    }

    @Override
    public void onBindViewHolder(BindViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        WidgetCheckBoxBinding binding = ((WidgetCheckBoxBinding) holder.getBinding());
        binding.checkbox.setClickable(false);
        binding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (position == 0) {
                for (int i = 1; i < items.size(); i++) {
                    items.get(i).isChecked.set(isChecked);
                }
            }
        });
    }

    @Override
    public void onItemClick(CheckBoxItem item, int position) {
        item.isChecked.set(!item.isChecked.get());
    }
}
