package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.StatRange;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.filterable.widget.range.FilterRangeData;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 25-Mar-17.
 */

public class FilterAttack implements Filterable {

    private final int selectedMin;
    private final int selectedMax;

    public FilterAttack(FilterRangeData data) {
        this.selectedMin = data.selectedStart;
        this.selectedMax = data.selectedEnd;
    }
    @Override
    public boolean isValid(Card card) {
        return card.getAttack() >= selectedMin && isValidMax(card.getAttack());
    }

    @Override
    public CardFilter getType() {
        return CardFilter.ATTACK;
    }

    private boolean isValidMax(int attack) {
        return selectedMax == StatRange.ATTACK.getMax() || attack <= selectedMax;
    }
}
