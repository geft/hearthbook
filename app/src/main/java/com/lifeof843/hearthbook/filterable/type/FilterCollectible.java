package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 09-Dec-17.
 */

public class FilterCollectible implements Filterable {

    private final boolean isCollectible;

    public FilterCollectible(boolean collectible) {
        this.isCollectible = collectible;
    }

    @Override
    public boolean isValid(Card card) {
        return isCollectible == card.isCollectible();
    }

    @Override
    public CardFilter getType() {
        return CardFilter.COLLECTIBLE;
    }
}
