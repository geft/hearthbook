package com.lifeof843.hearthbook.filterable.widget.range;

import com.lifeof843.hearthbook.core.CoreViewModel;

import org.parceler.Parcel;

/**
 * Created on 22-Mar-17.
 */

@Parcel
public class FilterRangeViewModel extends CoreViewModel {

    public FilterRangeData data;
}
