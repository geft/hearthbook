package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.CardGang;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 25-Mar-17.
 */

public class FilterGang implements Filterable {

    private final CardGang cardGang;

    public FilterGang(CardGang cardGang) {
        this.cardGang = cardGang;
    }

    @Override
    public boolean isValid(Card card) {
        return card.getGang() != null && card.getGang().equals(cardGang.toString());
    }

    @Override
    public CardFilter getType() {
        return CardFilter.GANG;
    }
}
