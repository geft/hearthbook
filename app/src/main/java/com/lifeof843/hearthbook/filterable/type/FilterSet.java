package com.lifeof843.hearthbook.filterable.type;

import com.lifeof843.hearthbook.constant.CardFilter;
import com.lifeof843.hearthbook.constant.CardSet;
import com.lifeof843.hearthbook.filterable.Filterable;
import com.lifeof843.hearthbook.model.Card;

/**
 * Created on 24-Mar-17.
 */

public class FilterSet implements Filterable {

    private final CardSet cardSet;

    public FilterSet(CardSet cardSet) {
        this.cardSet = cardSet;
    }

    @Override
    public boolean isValid(Card card) {
        return card.getSet() != null && card.getSet().equals(cardSet.toString());
    }

    @Override
    public CardFilter getType() {
        return CardFilter.SET;
    }
}
