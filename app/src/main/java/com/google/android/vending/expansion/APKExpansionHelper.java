package com.google.android.vending.expansion;

import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;

import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.lifeof843.hearthbook.R;
import com.lifeof843.hearthbook.expansion.ExpDownloaderService;
import com.lifeof843.hearthbook.util.ExceptionUtil;
import com.lifeof843.hearthbook.util.SnackBarUtil;

import java.util.ArrayList;
import java.util.List;

import static com.lifeof843.hearthbook.HearthbookApplication.EXT_SIZE_MAIN;
import static com.lifeof843.hearthbook.HearthbookApplication.EXT_VERSION_MAIN;

/**
 * Created on 02-Apr-17.
 */

public class APKExpansionHelper {

    private Context context;
    private PendingIntent pendingIntent;
    private List<XAPKFile> extFiles;

    public APKExpansionHelper(Context context, PendingIntent pendingIntent) {
        this.context = context;
        this.pendingIntent = pendingIntent;

        initXAPKS();
        checkExpansions();
    }

    private void initXAPKS() {
        XAPKFile main = new XAPKFile(true, EXT_VERSION_MAIN, EXT_SIZE_MAIN);

        extFiles = new ArrayList<>();
        extFiles.add(main);
    }

    private void checkExpansions() {
        if (!expansionFilesDelivered()) {
            if (isDownloadRequired(getStartDownloadResult())) {
                SnackBarUtil.show(context, R.string.home_download_required);
            }
        }
    }

    private boolean isDownloadRequired(int startResult) {
        return startResult != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED;
    }

    private int getStartDownloadResult() {
        int startResult = 0;

        try {
            startResult = DownloaderClientMarshaller.startDownloadServiceIfRequired(
                    context.getApplicationContext(), pendingIntent, ExpDownloaderService.class);
        } catch (PackageManager.NameNotFoundException e) {
            ExceptionUtil.log(e);
        }

        return startResult;
    }

    private boolean expansionFilesDelivered() {
        for (XAPKFile file : extFiles) {
            String fileName = Helpers.getExpansionAPKFileName(context, file.mIsMain, file.mFileVersion);
            if (!Helpers.doesFileExist(context, fileName, file.mFileSize, true))
                return false;
        }
        return true;
    }

    private static class XAPKFile {
        final boolean mIsMain;
        final int mFileVersion;
        final long mFileSize;

        XAPKFile(boolean isMain, int fileVersion, long fileSize) {
            mIsMain = isMain;
            mFileVersion = fileVersion;
            mFileSize = fileSize;
        }
    }
}
